﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports Telerik.Web.UI.GridExcelBuilder
Imports System.Drawing

Public Class ReportR0002
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        dsSales.ConnectionString = AppSettings("connstring")
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
        dsSales.SelectCommand = hfSQL.Value
        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub rgReport_ExcelMLWorkBookCreated(sender As Object, e As GridExcelMLWorkBookCreatedEventArgs) Handles rgReport.ExcelMLWorkBookCreated
        For Each row As RowElement In e.WorkBook.Worksheets(0).Table.Rows
            row.Cells(0).StyleValue = "Style1"
        Next

        Dim style As New StyleElement("Style1")
        style.InteriorStyle.Pattern = InteriorPatternType.Solid
        style.InteriorStyle.Color = System.Drawing.Color.LightGray
        e.WorkBook.Styles.Add(style)
    End Sub

    Private Sub rgDealer_InsertCommand(sender As Object, e As GridCommandEventArgs) Handles rgReport.InsertCommand
        If e.CommandName = RadGrid.ExportToExcelCommandName Then
            rgReport.ExportSettings.Excel.Format = GridExcelExportFormat.Biff
            rgReport.ExportSettings.IgnorePaging = True
            rgReport.ExportSettings.ExportOnlyData = True
            rgReport.ExportSettings.OpenInNewWindow = True
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClear_Click(sender As Object, e As EventArgs) Handles btnClear.Click
        txtCustState.Text = ""
        txtDealerNo.Text = ""
        txtDealerName.Text = ""
        txtVIN.Text = ""
        txtMake.Text = ""
        txtModel.Text = ""
        rdpSoldEnd.SelectedDate = Nothing
        rdpSoldStart.SelectedDate = Nothing
        rdpPaidDate.SelectedDate = Nothing
        rdpPaidEnd.SelectedDate = Nothing
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select c.ContractID, CustomerID, 
                                        CustomerFName, CustomerLName, CustomerAddr1, CustomerAddr2, CustomerCity, CustomerState, CustomerZip,
                                            CustomerPhone, AgentName, MGA, DealerNo, DealerName, ec.DealerCost, ec.CustomerCost, ec.Markup, TotalSurcharge,
                                            AgentFee, ClassFee, PPAFee, VSCPlan, SoldDate, Deductible, FINTerm, ec.TermMonth, ec.TermMile, DecForm, TC,
                                            ec.Class, ec.VIN, ec.ContractNo, ec.Year, ec.Make, ec.Model, ec.Trim, surDiesel, surHydraulic, sur4WD, surTurbo, surHybrid,
                                            surCommercial, surAirBladder, surLiftKit, surLiftKit78, surLuxury, surSnowPlow, surSeals, DealStatus,
                                            PaidDate, PaidStatus, BasePrice, DateAdded, DateUpdated, SaleMiles, Program, ec.EffDate, CancelDate,
                                            ec.EffMile, CancelDate, PlanCode, AgentNo, c.DatePaid
                                        from VeritasMoxy.dbo.epcontract ec
										left join contract c on ec.contractno = c.contractno "
        SQL = SQL + "where c.contractid > 0 "
        If txtCustState.Text.Length > 0 Then
            SQL = SQL + "and customerstate like '%" & txtCustState.Text & "%' "
        End If
        If txtDealerNo.Text.Length > 0 Then
            SQL = SQL + "and dealerno like '%" & txtDealerNo.Text & "%' "
        End If
        If txtDealerName.Text.Length > 0 Then
            SQL = SQL + "and dealername like '%" & txtDealerName.Text & "%' "
        End If
        If txtVIN.Text.Length > 0 Then
            SQL = SQL + "and ec.vin like '%" & txtVIN.Text & "%' "
        End If
        If txtMake.Text.Length > 0 Then
            SQL = SQL + "and ec.make like '%" & txtMake.Text & "%' "
        End If
        If txtModel.Text.Length > 0 Then
            SQL = SQL + "and ec.model like '%" & txtModel.Text & "%' "
        End If
        If Not rdpSoldStart.SelectedDate Is Nothing Then
            SQL = SQL + "and solddate >= '" & rdpSoldStart.SelectedDate & "' "
        End If
        If Not rdpSoldEnd.SelectedDate Is Nothing Then
            SQL = SQL + "and solddate < '" & DateAdd(DateInterval.Day, 1, CDate(rdpSoldEnd.SelectedDate)) & "' "
        End If
        If Not rdpPaidDate.SelectedDate Is Nothing Then
            SQL = SQL + "and datepaid >= '" & rdpPaidDate.SelectedDate & "' "
        End If
        If Not rdpPaidEnd.SelectedDate Is Nothing Then
            SQL = SQL + "and datepaid < '" & DateAdd(DateInterval.Day, 1, CDate(rdpPaidEnd.SelectedDate)) & "' "
        End If
        'rgReport.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        'rgReport.Rebind()
        dsSales.SelectCommand = SQL
        rgReport.Rebind()
        hfSQL.Value = SQL
        clR.OpenDB(SQL, AppSettings("connstring"))
        txtRecCnt.Text = Format(CLng(clR.RowCount), "#,##0")
    End Sub
End Class