﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DealerClaimsContract.aspx.vb" Inherits="VeritasGlobal.DealerClaimsContract" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaim">
                    <telerik:RadGrid ID="rgContract" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1000">
                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                            CommandItemDisplay="TopAndBottom">
                            <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                            <Columns>
                                <telerik:GridBoundColumn DataField="ContractID" UniqueName="ContractID" Visible="false"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="contractno" UniqueName="contractno" HeaderText="Contract No"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="SaleDate" UniqueName="SaleDate" HeaderText="Sale Date" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="Customer" UniqueName="Customer" HeaderText="Customer"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="status" UniqueName="status" HeaderText="status"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="PlanType" UniqueName="PlanType" HeaderText="Plan Type"></telerik:GridBoundColumn>
                                <telerik:GridBoundColumn DataField="EarnAmt" UniqueName="EarnAmt" HeaderText="Earn Amt" DataFormatString="{0:#,##0.00}"></telerik:GridBoundColumn>
                            </Columns>
                        </MasterTableView>
                    </telerik:RadGrid>
                </telerik:RadAjaxPanel>
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfDealerID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
