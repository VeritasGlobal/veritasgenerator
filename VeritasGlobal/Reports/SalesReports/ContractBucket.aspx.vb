﻿Imports Telerik.Web.UI
Imports System.Web.UI.WebControls
Imports xi = Telerik.Web.UI.ExportInfrastructure
Imports System.Web.UI
Imports System.Web
Imports Telerik.Web.UI.GridExcelBuilder
Imports System.Drawing

Public Class ContractBucket
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            rdpEnd.SelectedDate = Today
            rdpStart.SelectedDate = DateAdd(DateInterval.Day, -7, Today)
            fillcontracttype
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
        End If
    End Sub

    Private Sub FillContractType()
        cboContractType.Items.Add("All")
        cboContractType.Items.Add("Red Shield")
        cboContractType.Items.Add("Veritas")
        cboContractType.Items.Add("Express Protect")
        cboContractType.Items.Add("AutoNation")
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0018"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRunReport_Click(sender As Object, e As EventArgs) Handles btnRunReport.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select c.contractid, d.dealerno, d.dealername, d.state as DealerSTate, c.datepaid, a.agentno, a.AgentName, c.CustomerCost,  "
        SQL = SQL + "c.status, p.programname, pt0.PlanType, c.termmile, c.termmonth, "
        SQL = SQL + "case when dcc.dealercost is null then MoxyDealerCost else dcc.dealercost end as DealerCost, "
        SQL = SQL + "case when cr.amt is null then 0 else cr.amt end as ClaimReserve, "
        SQL = SQL + "case when sr.amt is null then 0 else sr.amt end as ReserveSurcharge, "
        SQL = SQL + "case when hl.amt is null then 0 else hl.amt end as Highline, "
        SQL = SQL + "case when awd.amt is null then 0 else awd.amt end as AWD, "
        SQL = SQL + "case when diesel.amt is null then 0 else diesel.amt end as Diesel, "
        SQL = SQL + "case when t.amt is null then 0 else t.amt end as Turbo, "
        SQL = SQL + "case when comm.amt is null then 0 else comm.amt end as Commercial, "
        SQL = SQL + "case when ab.amt is null then 0 else ab.amt end as AirBlatter, "
        SQL = SQL + "case when hyd.amt is null then 0 else hyd.amt end as Hydraulic, "
        SQL = SQL + "case when lux.amt is null then 0 else lux.amt end as Luxury, "
        SQL = SQL + "case when hyb.amt is null then 0 else hyb.amt end as Hybrid, "
        SQL = SQL + "case when lk.amt is null then 0 else lk.amt end as LiftKit, "
        SQL = SQL + "case when lkl.amt is null then 0 else lkl.amt end as LargeLiftKit, "
        SQL = SQL + "case when sg.amt is null then 0 else sg.amt end as Seals, "
        SQL = SQL + "case when sp.amt is null then 0 else sp.amt end as SnowPlow, "
        SQL = SQL + "case when ft.amt is null then 0 else ft.amt end as FullTerm, "
        SQL = SQL + "case when lt.amt is null then 0 else lt.amt end as LongTerm, "
        SQL = SQL + "case when lmf.amt is null then 0 else lmf.amt end as LMF, "
        SQL = SQL + "case when ded0.amt is null then 0 else ded0.amt end as Ded0, "
        SQL = SQL + "case when ded50.amt is null then 0 else ded50.amt end as Ded50, "
        SQL = SQL + "case when af.amt is null then 0 else af.amt end as AdminFee, "
        SQL = SQL + "case when r.amt is null then 0 else r.amt end as Roadside, "
        SQL = SQL + "case when s.amt is null then 0 else s.amt end as Software, "
        SQL = SQL + "case when m.amt is null then 0 else m.amt end as Moxy, "
        SQL = SQL + "case when pt.amt is null then 0 else pt.amt end as PremTax, "
        SQL = SQL + "case when cy.amt is null then 0 else cy.amt end as ContingencyFee, "
        SQL = SQL + "case when cf.amt is null then 0 else cf.amt end as ClipFee, "
        SQL = SQL + "case when o.amt is null then 0 else o.amt end as ObligorFee, "
        SQL = SQL + "case when pif.amt is null then 0 else pif.amt end as PIF, "
        SQL = SQL + "case when dcc.dealercost is null then MoxyDealerCost else dcc.dealercost end "
        SQL = SQL + "- case when cr.amt is null then 0 else cr.amt end "
        SQL = SQL + "- case when sr.amt is null then 0 else sr.amt end "
        SQL = SQL + "- case when af.amt is null then 0 else af.amt end "
        SQL = SQL + "- case when r.amt is null then 0 else r.amt end "
        SQL = SQL + "- case when s.amt is null then 0 else s.amt end "
        SQL = SQL + "- case when cy.amt is null then 0 else cy.amt end "
        SQL = SQL + "- case when cf.amt is null then 0 else cf.amt end "
        SQL = SQL + "- case when o.amt is null then 0 else o.amt end "
        SQL = SQL + "- case when m.amt is null then 0 else m.amt end "
        SQL = SQL + "- case when pif.amt is null then 0 else pif.amt end "
        SQL = SQL + "- case when awd.amt is null then 0 else awd.amt end "
        SQL = SQL + "- case when diesel.amt is null then 0 else diesel.amt end "
        SQL = SQL + "- case when t.amt is null then 0 else t.amt end "
        SQL = SQL + "- case when comm.amt is null then 0 else comm.amt end "
        SQL = SQL + "- case when ded0.amt is null then 0 else ded0.amt end "
        SQL = SQL + "- case when ded50.amt is null then 0 else ded50.amt end "
        SQL = SQL + "- case when ft.amt is null then 0 else ft.amt end "
        SQL = SQL + "- case when lt.amt is null then 0 else lt.amt end "
        SQL = SQL + "- case when lmf.amt is null then 0 else lmf.amt end "
        SQL = SQL + "- case when ab.amt is null then 0 else ab.amt end "
        SQL = SQL + "- case when hyd.amt is null then 0 else hyd.amt end "
        SQL = SQL + "- case when lux.amt is null then 0 else lux.amt end "
        SQL = SQL + "- case when hyb.amt is null then 0 else hyb.amt end "
        SQL = SQL + "- case when lk.amt is null then 0 else lk.amt end "
        SQL = SQL + "- case when sg.amt is null then 0 else sg.amt end "
        SQL = SQL + "- case when sp.amt is null then 0 else sp.amt end "
        SQL = SQL + "- case when hl.amt is null then 0 else hl.amt end "
        SQL = SQL + "- case when lkl.amt is null then 0 else lkl.amt end "
        SQL = SQL + "- case when pt.amt is null then 0 else pt.amt end as Commission "
        SQL = SQL + "from contract c "
        SQL = SQL + "left join vwclaimreserve cr on cr.contractid = c.ContractID "
        SQL = SQL + "left join vwSurchargeReserve sr on sr.contractid = c.contractid "
        SQL = SQL + "left join vwAdminFee AF on af.contractid = c.ContractID "
        SQL = SQL + "left join vwDealerCostCancel dcc on dcc.contractid = c.contractid "
        SQL = SQL + "left join vwRoadside R on r.contractid = c.ContractID "
        SQL = SQL + "left join vwSoftware S on s.contractid = c.ContractID "
        SQL = SQL + "left join vwPremTax PT on pt.contractid = c.ContractID "
        SQL = SQL + "left join vwContingency CY on cy.contractid = c.ContractID "
        SQL = SQL + "left join vwClipFee CF on cf.contractid = c.contractid "
        SQL = SQL + "left join vwObligorFee O on o.contractid = c.contractid "
        SQL = SQL + "left join vwMoxy M on m.contractid = c.ContractID "
        SQL = SQL + "left join vwPIF pif on pif.contractid = c.contractid "
        SQL = SQL + "left join vwAWD AWD on awd.contractid = c.ContractID "
        SQL = SQL + "left join vwDiesel Diesel on diesel.contractid = c.ContractID "
        SQL = SQL + "left join vwTurbo T on t.contractid = c.contractid "
        SQL = SQL + "left join vwCommercial comm on comm.contractid = c.contractid "
        SQL = SQL + "left join vwDed0 Ded0 on ded0.contractid = c.contractid "
        SQL = SQL + "left join vwDed50 Ded50 on ded50.contractid = c.contractid "
        SQL = SQL + "left join vwFullTerm FT on ft.contractid = c.contractid "
        SQL = SQL + "left join vwLongTerm LT on lt.contractid = c.contractid "
        SQL = SQL + "left join vwLMF lmf on lmf.contractid = c.contractid "
        SQL = SQL + "left join vwAirBlatter ab on ab.contractid = c.contractid "
        SQL = SQL + "left join vwHydraulic hyd on hyd.contractid = c.contractid "
        SQL = SQL + "left join vwLuxury lux on lux.contractid = c.contractid "
        SQL = SQL + "left join vwHybrid Hyb on hyb.contractid = c.contractid "
        SQL = SQL + "left join vwLiftKit lk on lk.contractid = c.contractid "
        SQL = SQL + "left join vwSeals SG on sg.contractid = c.contractid "
        SQL = SQL + "left join vwSnow SP on sp.contractid = c.contractid "
        SQL = SQL + "left join vwHighLine hl on hl.contractid = c.contractid "
        SQL = SQL + "left join vwLargeLiftKit lkl on lkl.contractid = c.contractid "
        SQL = SQL + "left join dealer d on d.dealerid = c.dealerid "
        SQL = SQL + "left join program p on p.ProgramID = c.ProgramID "
        SQL = SQL + "left join plantype pt0 on pt0.PlanTypeID = c.PlanTypeID "
        SQL = SQL + "left join Agents A on a.AgentID = c.AgentsID "
        SQL = SQL + "where not c.ProgramID in (47,48,51,54,55,56,57,58,59,60) "
        If Not rdpStart.SelectedDate Is Nothing Then
            SQL = SQL + "and c.datepaid >= '" & rdpStart.SelectedDate & "' "
        End If
        If Not rdpEnd.SelectedDate Is Nothing Then
            SQL = SQL + "and c.datepaid <= '" & rdpEnd.SelectedDate & "' "
        End If
        If cboContractType.Text = "Red Shield" Then
            SQL = SQL + "and (contractno like 'r%' or contractno like 'Vel%') "
        End If
        If cboContractType.Text = "Veritas" Then
            SQL = SQL + "and (contractno like 'v%' and not contractno like 'Vel%') "
        End If
        If cboContractType.Text = "Express Protect" Then
            SQL = SQL + "and dealerno like '2%' and dealerno like '%cc' "
        End If
        If cboContractType.Text = "AutoNation" Then
            SQL = SQL + "and dealerno like '2%' and not dealerno like '%cc' "
        End If
        SQL = SQL + "order by SaleDate desc "
        rgClaim.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
    End Sub
End Class