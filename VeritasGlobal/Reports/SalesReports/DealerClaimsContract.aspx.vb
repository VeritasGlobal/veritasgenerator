﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class DealerClaimsContract
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfDealerID.Value = Request.QueryString("dealerid")
        If Not IsPostBack Then
            GetServerInfo()
            FillGrid()
        End If
    End Sub

    Private Sub FillGrid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select contractid, contractno, SaleDate, fname + ' ' + lname as Customer, status,pt.PlanType, SaleDate, EarnAmt from contract c
        inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
        where dealerid = " & hfDealerID.Value
        rgContract.DataSource = clR.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub


End Class