﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="DealerClaimsSummary.aspx.vb" Inherits="VeritasGlobal.DealerClaimsSummary" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Table runat="server">
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Contract Earn Amt:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label ID="lblContractEarn" runat="server" Text="0.00"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Claims:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label ID="lblClaims" runat="server" Text="0.00"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell>
                    &nbsp
                </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell Font-Bold="true">
                    Loss Ratio:
                </asp:TableCell>
                <asp:TableCell>
                    <asp:Label ID="lblLossRatio" runat="server" Text="0.00"></asp:Label>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
        <asp:HiddenField ID="hfDealerID" runat="server" />
    </form>
</body>
</html>
