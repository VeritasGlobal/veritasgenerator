﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class DealerClaims60Day
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfDealerID.Value = Request.QueryString("dealerid")
        If Not IsPostBack Then
            GetServerInfo()
            FillGrid()
        End If
    End Sub

    Private Sub FillGrid()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select cl.claimid, contractno, c.status, c.SaleDate, fname + ' ' + LName as Customer, 
        pt.PlanType, claimno, lossdate, DATEDIFF(d, SaleDate, cl.CreDate) as days, 
        cl.LossMile - c.SaleMile as Miles, cd.ClaimDetailStatus, case when sum(paidamt) is null then 0  else sum(paidamt) end as PaidAmt 
        from claim cl
        inner join claimdetail cd on cd.claimid = cl.claimid
        inner join contract c on c.contractid = cl.contractid
        inner join ClaimReason cr on cr.ClaimReasonID = cd.ClaimReasonID
        inner join PlanType pt on pt.PlanTypeID = c.PlanTypeID
        where cl.ContractID in (select contractid from contract where dealerid = " & hfDealerID.Value
        SQL = SQL + ")
        and DATEDIFF(d, SaleDate, cl.CreDate) > 30 and DATEDIFF(d, SaleDate, cl.CreDate) <91
        group by contractno, claimno, cd.ClaimDetailStatus, lossdate, cl.CreDate, SaleDate,cd.ClaimReasonID, 
        SaleMile, LossMile, fname, lname,pt.PlanType,c.Status, c.SaleDate, cl.claimid "
        rgClaim.DataSource = clR.GetData(SQL, AppSettings("connstring"))
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub
End Class