﻿Imports System.Configuration.ConfigurationManager

Public Class AgentHourlyCallVolume
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
        End If
        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub btnPhoneReports_Click(sender As Object, e As EventArgs) Handles btnPhoneReports.Click
        Response.Redirect("~/reports/phonereports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        Dim SQL As String
        Dim clR As New clsDBO
        If rdpTo.SelectedDate Is Nothing Then
            Exit Sub
        End If
        If rdpFrom.SelectedDate Is Nothing Then
            Exit Sub
        End If
        SQL = "select ahcv.CallDate,  fname + ' ' + lname as AgentName,  case when active <> 0 then 'Active' else 'Not Active' end as Active,hourinterval, 
        Tot_InboundCalls as Inbound, 
		cast((Dur_InboundCalls / 60) as nvarchar(10)) + ':' + case when len(cast((dur_inboundcalls - ((Dur_InboundCalls /60) * 60)) as nvarchar(5))) =1 then '0' + 
		cast((dur_inboundcalls - ((Dur_InboundCalls /60) * 60)) as nvarchar(5)) 
		else 
		cast((dur_inboundcalls - ((Dur_InboundCalls /60) * 60)) as nvarchar(5)) end  as DurInbound,
        Tot_OutboundCalls as OutBound, 
		cast((Dur_OutboundCalls / 60) as nvarchar(10)) + ':' + 
		case when len(cast((dur_outboundcalls - ((Dur_outboundCalls /60) * 60)) as nvarchar(5))) = 1 then '0' + cast((dur_outboundcalls - ((Dur_outboundCalls /60) * 60)) as nvarchar(5))
		else
		cast((dur_outboundcalls - ((Dur_outboundCalls /60) * 60)) as nvarchar(5)) end as DurOutbound,  
        tot_internalcalls Internal, 
		cast((Dur_InternalCalls / 60) as nvarchar(10)) + ':' + 
		case when len(cast((dur_Internalcalls - ((Dur_InternalCalls /60) * 60)) as nvarchar(5))) = 1 then '0' + cast((dur_Internalcalls - ((Dur_InternalCalls /60) * 60)) as nvarchar(5)) 
		else
		cast((dur_Internalcalls - ((Dur_InternalCalls /60) * 60)) as nvarchar(5)) end as DurInternal from UserInfo ui
        inner join veritasphone.dbo.AgentHourlyCallVolume ahcv
        on ui.UserID = ahcv.UserID
        where ahcv.HourInterval >= 5
        and ahcv.HourInterval <= 19 "
        SQL = SQL + "and calldate >= '" & rdpFrom.SelectedDate & "' "
        SQL = SQL + "and calldate <= '" & rdpTo.SelectedDate & "' "
        rgPhone.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgPhone.Rebind()

    End Sub
End Class