﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimPaymentError.aspx.vb" Inherits="VeritasGlobal.ClaimPaymentError" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <telerik:RadGrid ID="rgClaim" runat="server" AutoGenerateColumns="false" AllowSorting="true" Width="1500" DataSourceID="dsPaymentError">
                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" DataKeyNames="claimid"
                        CommandItemDisplay="TopAndBottom">
                        <CommandItemSettings ShowExportToExcelButton="true" ShowExportToCsvButton="false" ShowAddNewRecordButton="false"/>
                        <Columns>
                            <telerik:GridBoundColumn DataField="ClaimID" UniqueName="ClaimID" Visible="false"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="ClaimNo" UniqueName="ClaimNo" HeaderText="Claim No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeNo" UniqueName="PayeeNo" HeaderText="Payee No"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="PaymentAmt" UniqueName="PaymentAmt" HeaderText="Payment Amt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                            <telerik:GridBoundColumn DataField="BillAmt" UniqueName="BillAmt" HeaderText="Bill Amt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                        </Columns>
                    </MasterTableView>
                    <ClientSettings EnablePostBackOnRowClick="true">
                        <Selecting AllowRowSelect="true" /> 
                    </ClientSettings>
                </telerik:RadGrid>
                <asp:SqlDataSource ID="dsPaymentError" ProviderName="System.Data.SqlClient"
                    selectcommand="select cl.claimid, cl.claimno, cp2.PayeeNo, PayeeName, cp.PaymentAmt, cp.BillAmt  from claim cl
                    inner join ClaimWexPayment cwp on cwp.ClaimID = cl.claimid
                    inner join claimpayment cp on cp.WexCode = cwp.WexCode
                    inner join ClaimPayee cp2 on cp.ClaimPayeeID = cp2.ClaimPayeeID
                    where cl.Status = 'Payment Error'"
                    runat="server"></asp:SqlDataSource>
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
