﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimAdjusterRank.aspx.vb" Inherits="VeritasGlobal.ClaimAdjusterRank" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>Claim Adjuster Rank</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:Table runat="server">
                                            <asp:TableRow>
                                                <asp:TableCell Font-Bold="true">
                                                    Start Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpStart" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    End Date:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <telerik:RadDatePicker ID="rdpEnd" runat="server"></telerik:RadDatePicker>
                                                </asp:TableCell>
                                                <asp:TableCell Font-Bold="true">
                                                    Contract Type:
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:DropDownList ID="cboContractType" DataSourceID="sdsContractType" DataTextField="contracttype" DataValueField="contracttype" runat="server"></asp:DropDownList>
                                                    <asp:SqlDataSource ID="sdsContractType" SelectCommand="select contracttype from veritasreports.dbo.claimrank group by contracttype order by contracttype " runat="server"></asp:SqlDataSource>
                                                </asp:TableCell>
                                                <asp:TableCell>
                                                    <asp:Button ID="btnRun" runat="server" BackColor="#1eabe2" Text="Run Report" />
                                                </asp:TableCell>
                                            </asp:TableRow>
                                        </asp:Table>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaim">
                                            <telerik:RadGrid ID="rgClaim" runat="server" AutoGenerateColumns="false" AllowSorting="true">
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true"
                                                    CommandItemDisplay="TopAndBottom">
                                                    <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="UserID" UniqueName="UserID" Visible="false"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="UserName" UniqueName="UserName" HeaderText="User Name"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Day3" UniqueName="Day3" HeaderText="Day 3"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Day5" UniqueName="Day5" HeaderText="Day 5"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Day7" UniqueName="Day7" HeaderText="Day 7"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="InspectComplete" UniqueName="InspectComplete" HeaderText="Inspect Complete"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClosedClaims" UniqueName="ClosedClaims" HeaderText="Closed Claims"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="NoReview" UniqueName="NoReview" HeaderText="No Review"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="approveamt" UniqueName="approveamt" HeaderText="Approve Amt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="approvecnt" UniqueName="approvecnt" HeaderText="Approve Cnt"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="approveavg" UniqueName="approveavg" HeaderText="Approve Avg" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="cmfamt" UniqueName="cmfamt" HeaderText="CMF Amt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="cmfcnt" UniqueName="cmfcnt" HeaderText="CMF Cnt"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="cmfavg" UniqueName="cmfavg" HeaderText="CMF Avg" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="billamt" UniqueName="billamt" HeaderText="Billing Amt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="billcnt" UniqueName="billcnt" HeaderText="Billing Cnt"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="billavg" UniqueName="billavg" HeaderText="Billing Avg" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="DeniedCnt" UniqueName="DeniedCnt" HeaderText="Denied Cnt"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
