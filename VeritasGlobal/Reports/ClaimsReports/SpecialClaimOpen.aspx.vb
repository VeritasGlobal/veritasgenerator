﻿Imports System
Imports System.Security.Claims
Imports DocumentFormat.OpenXml.Wordprocessing
Imports Telerik.Web.UI
Imports Telerik.Web.UI.GridExcelBuilder
Imports VeritasGlobalTools.FortePartsAPI

Public Class SpecialClaimOpen
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            rgClaim.Rebind()
            RunQuery()
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = True
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0032"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RunQuery()
    End Sub

    Private Sub RunQuery()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim lAgentID As Long
        Dim lSubAgentID As Long

        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            lAgentID = clR.Fields("AgentID")
            lSubAgentID = clR.Fields("SubagentID")
        End If

        SQL = "select cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate as Reportdate, 
            cl.Moddate as lastmaint, ca.ActivityDesc, vcdsta.totalamt as AmtDue, a.agentname, sa.subagentname,
			case when vcd.cnt is null then 0 else vcd.cnt end as ClaimAge, 
            case when vna.cnt is null then 0 else vna.cnt end as noactivity,
			sc.ServiceCenterName, cl.OpenDate, 
			case when DC.cnt is null then 0 else dc.cnt end as opendays,
			max(ci.RequestDate) as InspectionRequest,
			max(um.MessageDate) as InspectionComplete,
            max(case when ci.RequestDate is null then 0 
			else case when um.MessageDate is null then DATEDIFF(day, ci.requestdate, getdate()) else datediff(day, ci.requestdate, um.messagedate) end 
			end) as InspectionDays
			from claim cl
            inner join contract c on c.contractid = cl.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            left join agents a on d.agentsid = a.agentid
            left join SubAgents sa on sa.SubAgentID = d.SubAgentID
			left join vwClaimAge vcd on cl.ClaimID = vcd.claimid
			left join vwNoActivity vna on cl.claimid = vna.claimid
			left join vwclaimopen vco on cl.claimid = vco.claimid 
            left join ClaimActivity ca on ca.ClaimActivityID = cl.ClaimActivityID
            left join vwClaimDetailSumTotalAmt as VCDSTA on vcdsta.claimid = cl.claimid
            left join ServiceCenter sc on cl.ServiceCenterID = sc.ServiceCenterID
			left join ClaimInspection ci on ci.ClaimID = cl.ClaimID and not InspectionID is null
			left join UserMessage um on ci.InspectionID = um.InspectionID
            left join userinfo ui on ui.userid = cl.assignedto
			left join (select claimid, sum(case when CloseDate is null then DATEDIFF(day, OpenDate, getdate()) else DATEDIFF(day, OpenDate, CloseDate) end) as cnt from ClaimOpenHistory
            group by claimid) DC on cl.claimid = dc.claimid
            where ((cl.CloseDate is null
            and not cl.ClaimActivityID in (11,14,15,17,18,21,22,23,24,25,26,28,29,30,31,32,33,35,36,37,38,39,40,42,43,45,46,47,48)
            and cl.Status = 'Open')
            or cl.claimid in (select claimid from vwClaimDetailRequested)) "
        If lAgentID > 0 Then
            SQL = SQL + "and d.agentsid = " & lAgentID & " "
        End If
        If lSubAgentID > 0 Then
            SQL = SQL + "and d.subagentid = " & lSubAgentID & " "
        End If
        SQL = SQL + "group by cl.ClaimID, d.dealerno, DealerName, contractno, ui.username, c.FName, c.lname, vin, claimno, cl.CreDate, 
            cl.Moddate, ca.ActivityDesc, vcdsta.totalamt, a.agentname, sa.subagentname,
			case when DC.cnt is null then 0 else dc.cnt end,
            case when vna.cnt is null then 0 else vna.cnt end,
			sc.ServiceCenterName, cl.OpenDate, 
			case when vco.cnt is null then 0 else vco.cnt end,
			case when vcd.cnt is null then 0 else vcd.cnt end
			order by ClaimAge desc "
        rgClaim.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        lblRecordCount.Text = clR.RowCount
    End Sub

    Private Sub rgClaim_DataBound(sender As Object, e As EventArgs) Handles rgClaim.DataBound
        Dim cnt As Integer
        For cnt = 0 To rgClaim.Items.Count - 1
            If IsNumeric(rgClaim.Items(cnt).Item("ClaimAge").Text) Then
                If rgClaim.Items(cnt).Item("ClaimAge").Text = 3 Then
                    rgClaim.Items(cnt).Item("ClaimAge").BackColor = System.Drawing.Color.Yellow
                End If
                If rgClaim.Items(cnt).Item("ClaimAge").Text = 4 Then
                    rgClaim.Items(cnt).Item("ClaimAge").BackColor = System.Drawing.Color.Yellow
                End If
                If rgClaim.Items(cnt).Item("ClaimAge").Text = 5 Then
                    rgClaim.Items(cnt).Item("ClaimAge").BackColor = System.Drawing.Color.Orange
                End If
                If rgClaim.Items(cnt).Item("ClaimAge").Text = 6 Then
                    rgClaim.Items(cnt).Item("ClaimAge").BackColor = System.Drawing.Color.Orange
                End If
                If rgClaim.Items(cnt).Item("ClaimAge").Text > 6 Then
                    rgClaim.Items(cnt).Item("ClaimAge").BackColor = System.Drawing.Color.Red
                End If
            End If
            If IsNumeric(rgClaim.Items(cnt).Item("InspectionDays").Text) Then
                If rgClaim.Items(cnt).Item("InspectionDays").Text > 2 Then
                    rgClaim.Items(cnt).Item("InspectionDays").BackColor = System.Drawing.Color.Red
                End If
            End If
        Next
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class