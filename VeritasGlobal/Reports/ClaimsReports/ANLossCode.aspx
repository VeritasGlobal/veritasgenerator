﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ANLossCode.aspx.vb" Inherits="VeritasGlobal.ANLossCode" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title>AN Loss Code Report</title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:Panel runat="server" ID="pnlHeader" BackColor="#1eabe2" Height="100">
            <asp:Image ID="Image1" ImageUrl="~\images\Veritas_Final-Logo_550px.png" BackColor="#1eabe2" runat="server" />
            <asp:Table runat="server" ID="tbTodo" HorizontalAlign="Right"> 
                <asp:TableRow>
                    <asp:TableCell HorizontalAlign="Right">
                        <asp:HyperLink ID="hlToDo" Target="_blank" ImageUrl="~\images\TD_Icon.png" NavigateUrl="~\users\todoreader.aspx" runat="server"></asp:HyperLink>
                    </asp:TableCell>
                </asp:TableRow>
            </asp:Table>
        </asp:Panel>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Panel ID="pnlMenu" runat="server" Width="250" BackColor="Black" Height="825">
                                <asp:Table runat="server" Width="250">
                                    <asp:TableRow>
                                        <asp:TableCell ForeColor="White" HorizontalAlign="Center" Font-Names="Arial">
                                            Menu
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnHome" runat="server" Text="Home" ForeColor="White" EnableEmbeddedSkins="false" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/home.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSalesReports" runat="server" Text="Sales Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaimsReports" runat="server" Text="Claims Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccountingReports" runat="server" Text="Accounting Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnCustomReports" runat="server" Text="Custom Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <hr />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAgents" runat="server" Text="Agents"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Agent.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnDealer" runat="server" Text="Dealerships"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Dealers.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnContract" runat="server" Text="Contracts"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/contracts.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnClaim" runat="server" Text="Claims"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/claims.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnAccounting" runat="server" Text="Accounting"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/accounting.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnReports" runat="server" Text="Reports"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Reports.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnSettings" runat="server" Text="Settings"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/settings.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnUsers" runat="server" Text="Users"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Users.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <telerik:RadButton ID="btnLogOut" runat="server" Text="Log Out"  EnableEmbeddedSkins="false" ForeColor="White" BackColor="Black">
                                                <Icon PrimaryIconUrl="~/images/Logout.jpg" PrimaryIconWidth="15" />
                                            </telerik:RadButton>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:Panel>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="btnExport">
                                            <telerik:RadGrid ID="rgDealer" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                                AllowSorting="true" AllowPaging="false" ShowFooter="false"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small"  DataSourceID="sdsANClaimPart">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" /> 
                                                </ClientSettings>
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" DataKeyNames="idx">
                                                    <CommandItemSettings ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="idx" ReadOnly="true" Visible="false" UniqueName="idx"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartCode" UniqueName="PartCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartDesc" UniqueName="PartDesc" AllowFiltering="True" HeaderText="Sold Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimCount" UniqueName="ClaimCount" HeaderText="Claim Count"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimAmt" UniqueName="ClaimAmt" HeaderText="Claim Amt" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimAvg" UniqueName="ClaimAvg" HeaderText="Claim Avg" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="sdsANClaimPart"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select idx, partcode, partdesc, claimcount, claimamt, 
                                                claimavg from anclaimpart order by partcode, partdesc "  runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        &nbsp
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <telerik:RadAjaxPanel ID="RadAjaxPanel2" runat="server" PostBackControls="btnExport">
                                            <telerik:RadGrid ID="rgANClaimPart" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="false" 
                                                AllowSorting="true" AllowPaging="false" ShowFooter="false"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                                    Font-Names="Calibri" Font-Size="Small" DataSourceID="sdsByPayee">
                                                <ClientSettings>
                                                    <Scrolling AllowScroll="true" UseStaticHeaders="true" /> 
                                                </ClientSettings>
                                                <GroupingSettings CaseSensitive="false" />
                                                <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true">
                                                    <CommandItemSettings ShowExportToCsvButton="true" ShowAddNewRecordButton="false" />
                                                    <Columns>
                                                        <telerik:GridBoundColumn DataField="idx" ReadOnly="true" Visible="false" UniqueName="idx"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Payee" UniqueName="Payee" HeaderText="Payee"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="Contact" UniqueName="Contact" HeaderText="Contact"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="PartDesc" UniqueName="PartDesc" AllowFiltering="True" HeaderText="Sold Date" ItemStyle-Width="100" HeaderStyle-Width ="100" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimCount" UniqueName="ClaimCount" HeaderText="Claim Count"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimAmt" UniqueName="ClaimAmt" HeaderText="Claim Amt" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                        <telerik:GridBoundColumn DataField="ClaimAvg" UniqueName="ClaimAvg" HeaderText="Claim Avg" ItemStyle-Width="75" HeaderStyle-Width="75" DataFormatString="{0:C2}"></telerik:GridBoundColumn>
                                                    </Columns>
                                                </MasterTableView>
                                            </telerik:RadGrid>
                                            <asp:SqlDataSource ID="sdsByPayee"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select idx, payee, contact, partcode, partdesc, claimcount, claimamt, 
                                                claimavg from anclaimpartbypayee order by payee, contact, partcode"  runat="server"></asp:SqlDataSource>
                                        </telerik:RadAjaxPanel>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </asp:UpdatePanel>

        <asp:HiddenField ID="hfError" runat="server" />
        <telerik:RadWindow ID="rwError"  runat="server" Width="500" Height="150" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
            <ContentTemplate>
                <asp:Table runat="server" Height="60">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Label runat="server" ID="lblError" Text=""></asp:Label>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:Table runat="server" Width="400"> 
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Button ID="btnErrorOK" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="OK" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
            </ContentTemplate>
        </telerik:RadWindow>
        <asp:HiddenField ID="hfID" runat="server" />
        <asp:HiddenField ID="hfUserID" runat="server" />
    </form>
</body>
</html>
