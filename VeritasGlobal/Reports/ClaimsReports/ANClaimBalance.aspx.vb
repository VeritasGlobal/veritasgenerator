﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ANClaimBalance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsClaimBalance.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            GetDeposit()
            GetWithdraw()
            rgClaim.Rebind()
        End If
    End Sub

    Private Sub GetWithdraw()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(withdraw) as Withdraw from (
            select c.DatePaid as TransDate, 'AN CMF' as TransName, sum(ca.amt) as deposit, 0 as Withdraw from ContractAmt ca
            inner join contract c on c.contractid = ca.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            where ratetypeid = 1101
            and c.Status = 'Paid'
            and d.DealerNo like '2%'
            and not datepaid is null
            group by c.DatePaid
            union
            select cc.CancelDate as TransDate, 'AN CMF Cancel' as TransName, 0 as Deposit, sum(ca.CancelAmt) as Withdraw from ContractAmt ca
            inner join contract c on c.contractid = ca.ContractID
            left join contractcancel cc on cc.contractid = c.ContractID
            inner join dealer d on c.DealerID = d.DealerID
            where ratetypeid = 1101
            and c.Status = 'Cancelled'
            and not CancelDate is null
            group by cc.CancelDate
            union
            select DatePaid as TransDate, 'Claims' as TransName, 0 as Deposit, sum(TotalAmt) as Withdraw from claimdetail cd
            where ClaimDetailStatus = 'Paid'
            and RateTypeID = 1101
            group by datepaid) ca "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("withdraw").Length > 0 Then
                txtWithdraw.Text = clR.Fields("withdraw")
            End If
        End If
    End Sub

    Private Sub GetDeposit()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(deposit) as Deposit from (
            select c.DatePaid as TransDate, 'AN CMF' as TransName, sum(ca.amt) as deposit, 0 as Withdraw from ContractAmt ca
            inner join contract c on c.contractid = ca.ContractID
            where ratetypeid = 1101
            and c.Status = 'Paid'
            and not datepaid is null
            group by c.DatePaid
            union
            select cc.CancelDate as TransDate, 'AN CMF Cancel' as TransName, 0 as Deposit, sum(ca.CancelAmt) as Withdraw from ContractAmt ca
            inner join contract c on c.contractid = ca.ContractID
            left join contractcancel cc on cc.contractid = c.ContractID
            where ratetypeid = 1101
            and c.Status = 'Cancelled'
            and not CancelDate is null
            group by cc.CancelDate
            union
            select DatePaid as TransDate, 'Claims' as TransName, 0 as Deposit, sum(TotalAmt) as Withdraw from claimdetail
            where ClaimDetailStatus = 'Paid'
            and RateTypeID = 1101
            group by datepaid) ca "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("deposit").Length > 0 Then
                txtDeposit.Text = clR.Fields("deposit")
            End If
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0050"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub


End Class