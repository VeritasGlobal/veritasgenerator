﻿Imports Telerik.Web.UI

Public Class ANRMF
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = System.Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            rgClaim.Rebind()
            RunQuery()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToExcel" Then
            rgClaim.ExportSettings.ExportOnlyData = True
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            rgClaim.ExportSettings.Excel.FileExtension = "xlsx"
            rgClaim.ExportSettings.FileName = "R0054"
            rgClaim.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToExcel()
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRefresh_Click(sender As Object, e As EventArgs) Handles btnRefresh.Click
        RunQuery()
    End Sub

    Private Sub RunQuery()
        Dim SQL As String
        Dim clR As New clsDBO

        SQL = "select claimid, claimno, contractno, dealerno, dealername, sum(anrmf) as ANRMF, sum(reserve) as Reserve from (
        select cl.claimid, claimno, contractno, dealerno, dealername, sum(cd.TotalAmt) as ANRMF, 0 as Reserve from claim cl
        inner join contract c on cl.contractid = c.contractid
        inner join dealer d on c.dealerid = d.dealerid
        inner join claimdetail cd on cd.claimid = cl.claimid
        where cd.RateTypeID = 1101
        and (cd.ClaimDetailStatus = 'Authorized'
        or cd.ClaimDetailStatus = 'Paid'
        or cd.ClaimDetailStatus = 'transmitted'
        or cd.ClaimDetailStatus = 'Approved')
        group by claimno, dealerno,contractno, dealername, cl.ClaimID
        union
        select cl.claimid, claimno, contractno, dealerno, dealername,  0 as ANRMF, sum(cd.TotalAmt) as Reserve from claim cl
        inner join contract c on cl.contractid = c.contractid
        inner join dealer d on c.dealerid = d.dealerid
        inner join claimdetail cd on cd.claimid = cl.claimid
        where cd.RateTypeID = 1
        and (cd.ClaimDetailStatus = 'Authorized'
        or cd.ClaimDetailStatus = 'Paid'
        or cd.ClaimDetailStatus = 'transmitted'
        or cd.ClaimDetailStatus = 'Approved')
        and dealerno like '2%'
        and not dealerno like '%cc'
        group by claimno, dealerno,contractno, dealername, cl.ClaimID) an
        group by claimno, dealerno,contractno, dealername,claimid "
        rgClaim.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring")).Tables(0)
        rgClaim.Rebind()
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        lblRecordCount.Text = clR.RowCount
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub

End Class