﻿Imports Telerik.Web.UI

Public Class ClaimsReports
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        GetServerInfo()
        CheckToDo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            FillReports()
        End If

        If hfError.Value = "Visible" Then
            rwError.VisibleOnPageLoad = True
        Else
            rwError.VisibleOnPageLoad = False
        End If
    End Sub

    Private Sub FillReports()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select reportid, reportno, reportname 
                            from reports where reportcategory = 'Claims' "
        If checkanonly Then
            SQL = SQL + "and reportid in (1032,1034,1068) "
        End If
        If hfVeroOnly.Value = "True" Then
            SQL = SQL + "and reportid in (1042,1068) "
        End If
        If hfAgentID.Value.Length > 0 Then
            If hfAgentID.Value <> "0" Then
                SQL = SQL + "and reportid in (1031,1033,1068) "
            End If
        End If
        rgReport.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgReport.Rebind()
    End Sub

    Private Function CheckANOnly() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        hfVeroOnly.Value = "False"
        CheckANOnly = False
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and autonationonly <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckANOnly = True
        End If
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and veroonly <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            hfVeroOnly.Value = "True"
        End If
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfAgentID.Value = clR.Fields("agentid")
            hfSubAgentID.Value = clR.Fields("subagentid")
        End If
    End Function

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnSalesReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("phonereports") = True Then
                btnPhoneReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnAccountingReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnClaimsReports.Enabled = True
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnCustomReports.Enabled = True
                btnReports.Enabled = True
            End If
        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnClaimsReports.Enabled = False
        btnSalesReports.Enabled = False
        btnAccountingReports.Enabled = False
        btnCustomReports.Enabled = False
        btnPhoneReports.Enabled = False
    End Sub

    Private Sub rgReport_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgReport.SelectedIndexChanged
        If rgReport.SelectedValue = 1006 Then
            Response.Redirect("~/reports/claimsreports/ClaimStats.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1007 Then
            Response.Redirect("~/reports/claimsreports/ClaimPendingPaymentAge.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1011 Then
            Response.Redirect("~/reports/claimsreports/ANLossCode.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1012 Then
            Response.Redirect("~/reports/claimsreports/SingleMultipleClaim.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1017 Then
            Response.Redirect("~/reports/claimsreports/PaidDenied.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1018 Then
            Response.Redirect("~/reports/claimsreports/ANClaim.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1020 Then
            Response.Redirect("~/reports/claimsreports/ClaimApproveMonth.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1021 Then
            Response.Redirect("~/reports/claimsreports/ClaimApprovedWeek.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1022 Then
            Response.Redirect("~/reports/claimsreports/ClaimPaidReport.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1026 Then
            Response.Redirect("~/reports/claimsreports/ClaimApproveDaily.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1027 Then
            Response.Redirect("~/reports/claimsreports/ClaimsOpenAll.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1028 Then
            Response.Redirect("~/reports/claimsreports/OpenedClaims.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1029 Then
            Response.Redirect("~/reports/claimsreports/ClaimAccess.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1030 Then
            Response.Redirect("~/reports/claimsreports/ClaimAdjusterRank.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1031 Then
            Response.Redirect("~/reports/claimsreports/ClaimsOpenLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1032 Then
            Response.Redirect("~/reports/claimsreports/ANClaimOpenLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1033 Then
            Response.Redirect("~/reports/claimsreports/ClaimOpenApprovedLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1034 Then
            Response.Redirect("~/reports/claimsreports/ANClaimOpenApprovedLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1035 Then
            Response.Redirect("~/reports/claimsreports/ClaimOpenLiveByTeams.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1036 Then
            Response.Redirect("~/reports/claimsreports/ANClaimOpenLiveByTeam.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1037 Then
            Response.Redirect("~/reports/claimsreports/ClaimOpenApproveLiveByTeam.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1038 Then
            Response.Redirect("~/reports/claimsreports/ANClaimOpenApproveLiveByTeam.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1039 Then
            Response.Redirect("~/reports/claimsreports/MonthEndClaimReserveCMF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1040 Then
            Response.Redirect("~/reports/claimsreports/ClaimDataPoint.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1041 Then
            Response.Redirect("~/reports/claimsreports/ServiceCenterClaim.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1042 Then
            Response.Redirect("~/reports/claimsreports/VeroClaimOpenLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1043 Then
            Response.Redirect("~/reports/claimsreports/ClaimAuthorized.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1044 Then
            Response.Redirect("~/reports/claimsreports/ClaimsOnline.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1045 Then
            Response.Redirect("~/reports/claimsreports/ClaimCMF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1046 Then
            Response.Redirect("~/reports/claimsreports/SpecialClaimOpen.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1047 Then
            Response.Redirect("~/reports/claimsreports/OpenInspection.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1048 Then
            Response.Redirect("~/reports/claimsreports/WebClaimOpen.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1049 Then
            Response.Redirect("~/reports/claimsreports/ANClaimBalance.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1051 Then
            Response.Redirect("~/reports/claimsreports/ClaimPaymentError.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1052 Then
            Response.Redirect("~/reports/claimsreports/AZClaimOpenLive.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1053 Then
            Response.Redirect("~/reports/claimsreports/ANRMF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1054 Then
            Response.Redirect("~/reports/claimsreports/ANClaimsOpenWaitingRF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1055 Then
            Response.Redirect("~/reports/claimsreports/ClaimOpenWaitingRF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1056 Then
            Response.Redirect("~/reports/claimsreports/ClaimRMF.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1062 Then
            Response.Redirect("~/reports/claimsreports/AgentClaimCnt.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1063 Then
            Response.Redirect("~/reports/claimsreports/VerifyClaimStatusAN.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1064 Then
            Response.Redirect("~/reports/claimsreports/VerifyClaimStatus.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1068 Then
            Response.Redirect("~/reports/claimsreports/ClaimReportByDealer.aspx?sid=" & hfID.Value)
        End If
        If rgReport.SelectedValue = 1069 Then
            Response.Redirect("~/reports/claimsreports/SeverityDetail.aspx?sid=" & hfID.Value)
        End If

    End Sub

    Private Sub btnPhoneReports_Click(sender As Object, e As EventArgs) Handles btnPhoneReports.Click
        Response.Redirect("~/reports/phonereports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSalesReports_Click(sender As Object, e As EventArgs) Handles btnSalesReports.Click
        Response.Redirect("~/reports/salesreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsReports_Click(sender As Object, e As EventArgs) Handles btnClaimsReports.Click
        Response.Redirect("~/reports/claimsreports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccountingReports_Click(sender As Object, e As EventArgs) Handles btnAccountingReports.Click
        Response.Redirect("~/reports/AccountingReports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub


End Class