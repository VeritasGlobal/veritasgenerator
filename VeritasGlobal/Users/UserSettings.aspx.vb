﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports VeritasGlobalTools

Public Class UserSettings
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsUser.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        GetServerInfo()
        If Not IsPostBack Then
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            pnlSearch.Visible = True
            pnlDetail.Visible = False
            FillInsCarrier()
            FillTeams()
            FillAgents()
        End If
    End Sub

    Private Sub FillAgents()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim li As New ListItem
        li.Value = 0
        li.Text = "All"
        cboAgent.Items.Add(li)
        SQL = "select * from agents "
        SQL = SQL + "where not agentno is null "
        SQL = SQL + "and statusid = 2 "
        SQL = SQL + "order by agentname "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("agentid")
                li.Text = clR.Fields("agentname")
                cboAgent.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub FillInsCarrier()
        Dim li As New ListItem
        li.Value = 0
        li.Text = "All"
        cboInsCarrier.Items.Add(li)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from inscarrier "
        SQL = SQL + "order by inscarrierid "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("inscarrierid")
                li.Text = clR.Fields("inscarriername")
                cboInsCarrier.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub FillTeams()
        Dim li As New ListItem
        li.Value = 0
        li.Text = ""
        cboTeams.Items.Add(li)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from userteam "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("teamid")
                li.Text = clR.Fields("teamname")
                cboTeams.Items.Add(li)
            Next
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUserSettings.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("phonereports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("usersettings") = True Then
                btnUserSettings.Enabled = True
            End If

        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUserSettings_Click(sender As Object, e As EventArgs) Handles btnUserSettings.Click
        Response.Redirect("~/users/usersettings.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnChangePassword_Click(sender As Object, e As EventArgs) Handles btnChangePassword.Click
        Response.Redirect("~/users/ChangePassword.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlSearch.Visible = False
        pnlDetail.Visible = True
        hfUserDetailID.Value = 0
        ClearFields()
    End Sub

    Private Sub ClearFields()
        txtFName.Text = ""
        txtLName.Text = ""
        txtEMail.Text = ""
        txtPassword.Text = ""
        txtClaimMax.Text = 0
        txtMaxAuth.Text = 0
        chkActive.Checked = False
        chkUserSettings.Checked = False
        chkAccounting.Checked = False
        chkSettings.Checked = False
        chkAgents.Checked = False
        chkDealer.Checked = False
        chkClaim.Checked = False
        txtClaimMax.Text = 0
        chkContract.Checked = False
        chkSaleReport.Checked = False
        chkClaimsReports.Checked = False
        chkAccountReports.Checked = False
        chkCustomReports.Checked = False
        chkCancel.Checked = False
        chkCancelGap.Checked = False
        chkCancelMod.Checked = False
        chkContractMod.Checked = False
        chkHCCApprovalCEO.Checked = False
        chkHCCApprovalManager.Checked = False
        chkPhoneReports.Checked = False
        cboTeams.SelectedValue = 0
        chkTeamLead.Checked = False
        chkANOnly.Checked = False
        chkVeroOnly.Checked = False
        chkCancel100.Checked = False
        chkViewBreakdown.Checked = False
        chkTotalPayment.Checked = False
        chkAllow45Days.Checked = False
        chkAllowAZDenied.Checked = False
        chkCES.Checked = False
    End Sub

    Private Sub rgUser_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgUser.SelectedIndexChanged
        hfUserDetailID.Value = rgUser.SelectedValue
        ClearFields()
        FillUser()
        pnlDetail.Visible = True
        pnlSearch.Visible = False

    End Sub

    Private Sub FillUser()
        Dim SQL As String
        Dim clU As New clsDBO
        Dim clSI As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserDetailID.Value
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            txtEMail.Text = clU.Fields("email")
            txtFName.Text = clU.Fields("fname")
            txtLName.Text = clU.Fields("lname")
            txtPassword.Text = clU.Fields("password")
            chkActive.Checked = CBool(clU.Fields("active"))
        End If

        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserDetailID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            chkUserSettings.Checked = CBool(clSI.Fields("usersettings"))
            chkAccounting.Checked = CBool(clSI.Fields("Accounting"))
            chkSettings.Checked = CBool(clSI.Fields("settings"))
            chkAgents.Checked = CBool(clSI.Fields("agents"))
            chkDealer.Checked = CBool(clSI.Fields("dealer"))
            chkActivateContract.Checked = CBool(clSI.Fields("activatecontract"))
            chkClaim.Checked = CBool(clSI.Fields("claim"))
            chkContract.Checked = CBool(clSI.Fields("contract"))
            chkSaleReport.Checked = CBool(clSI.Fields("salesreports"))
            chkClaimsReports.Checked = CBool(clSI.Fields("claimsreports"))
            chkAccountReports.Checked = CBool(clSI.Fields("accountreports"))
            chkCustomReports.Checked = CBool(clSI.Fields("customreports"))
            chkCancelMod.Checked = CBool(clSI.Fields("cancelmodification"))
            chkCancelGap.Checked = CBool(clSI.Fields("cancelgap"))
            chkCancel.Checked = CBool(clSI.Fields("cancellation"))
            chkHCCApprovalManager.Checked = CBool(clSI.Fields("hccapprovalManager"))
            chkHCCApprovalCEO.Checked = CBool(clSI.Fields("hccapprovalCEO"))
            chkCancelPayment.Checked = CBool(clSI.Fields("CancelPay"))
            chkUnlockClaim.Checked = CBool(clSI.Fields("UnlockClaim"))
            chkClaimPayment.Checked = CBool(clSI.Fields("ClaimPayment"))
            txtClaimMax.Text = clSI.Fields("claimapprove")
            txtMaxAuth.Text = clSI.Fields("claimauth")
            cboInsCarrier.SelectedValue = clSI.Fields("inscarrierid")
            chkReadOnly.Checked = clSI.Fields("readonly")
            chkContractMod.Checked = clSI.Fields("contractmodification")
            chkTeamLead.Checked = CBool(clSI.Fields("TeamLead"))
            chkANOnly.Checked = CBool(clSI.Fields("autonationonly"))
            chkVeroOnly.Checked = CBool(clSI.Fields("veroonly"))
            cboAgent.SelectedValue = clSI.Fields("agentid")
            cboSubAgent.SelectedValue = clSI.Fields("subagentid")
            chkAllowCancelExpire.Checked = CBool(clSI.Fields("allowcancelexpire"))
            chkWithIn3Days.Checked = CBool(clSI.Fields("within3days"))
            chkAllCoverage.Checked = CBool(clSI.Fields("allcoverage"))
            chkAllowSlush.Checked = CBool(clSI.Fields("allowslush"))
            chkCancel100.Checked = CBool(clSI.Fields("cancel100"))
            chkViewBreakdown.Checked = CBool(clSI.Fields("viewbreakdown"))
            chkAllowUndeny.Checked = CBool(clSI.Fields("AllowUndenyClaim"))
            chkClaimAudit.Checked = CBool(clSI.Fields("allowclaimaudit"))
            chkBypassMissingInfo.Checked = CBool(clSI.Fields("bypassmissinginfo"))
            chkAllow45Days.Checked = CBool(clSI.Fields("allow45days"))
            If clSI.Fields("TeamID").Length = 0 Then
                clSI.Fields("TeamID") = 0
            End If
            cboTeams.SelectedValue = clSI.Fields("TeamID")
            chkTotalPayment.Checked = CBool(clSI.Fields("allowtotalpayment"))
            chkAllowAZDenied.Checked = CBool(clSI.Fields("allowazdenied"))
            chkPhoneReports.Checked = CBool(clSI.Fields("phonereports"))
            chkCES.Checked = CBool(clSI.Fields("ces"))
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clUSI As New clsDBO
        Dim clU As New clsDBO
        SQL = "select * from userinfo "
        If hfUserDetailID.Value.Length = 0 Then
            SQL = SQL + "where userid = 0 "
        Else
            SQL = SQL + "where userid = " & hfUserDetailID.Value
        End If
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
        Else
            clU.NewRow()
        End If
        clU.Fields("email") = txtEMail.Text
        clU.Fields("password") = txtPassword.Text
        clU.Fields("fname") = txtFName.Text
        clU.Fields("lname") = txtLName.Text
        clU.Fields("active") = chkActive.Checked
        If clU.RowCount = 0 Then
            clU.AddRow()
        End If
        clU.SaveDB()
        GetUserInfo()
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserDetailID.Value
        clUSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clUSI.RowCount > 0 Then
            clUSI.GetRow()
        Else
            clUSI.NewRow()
        End If
        clUSI.Fields("userid") = hfUserDetailID.Value
        clUSI.Fields("usersettings") = chkUserSettings.Checked
        clUSI.Fields("accounting") = chkAccounting.Checked
        clUSI.Fields("settings") = chkSettings.Checked
        clUSI.Fields("agents") = chkAgents.Checked
        clUSI.Fields("dealer") = chkDealer.Checked
        clUSI.Fields("claim") = chkClaim.Checked
        clUSI.Fields("unlockClaim") = chkUnlockClaim.Checked
        clUSI.Fields("contract") = chkContract.Checked
        clUSI.Fields("salesreports") = chkSaleReport.Checked
        clUSI.Fields("activatecontract") = chkActivateContract.Checked
        clUSI.Fields("claimsreports") = chkClaimsReports.Checked
        clUSI.Fields("accountreports") = chkAccountReports.Checked
        clUSI.Fields("customreports") = chkCustomReports.Checked
        clUSI.Fields("Cancellation") = chkCancel.Checked
        clUSI.Fields("cancelgap") = chkCancelGap.Checked
        clUSI.Fields("cancelmodification") = chkCancelMod.Checked
        clUSI.Fields("hccapprovalmanager") = chkHCCApprovalManager.Checked
        clUSI.Fields("hccapprovalceo") = chkHCCApprovalCEO.Checked
        clUSI.Fields("claimpayment") = chkClaimPayment.Checked
        clUSI.Fields("cancelpay") = chkCancelPayment.Checked
        clUSI.Fields("claimauth") = txtMaxAuth.Text
        clUSI.Fields("claimapprove") = txtClaimMax.Text
        clUSI.Fields("readonly") = chkReadOnly.Checked
        clUSI.Fields("inscarrierid") = cboInsCarrier.SelectedValue
        clUSI.Fields("contractmodification") = chkContractMod.Checked
        clUSI.Fields("teamid") = cboTeams.SelectedValue
        clUSI.Fields("teamlead") = chkTeamLead.Checked
        clUSI.Fields("autonationonly") = chkANOnly.Checked
        clUSI.Fields("veroonly") = chkVeroOnly.Checked
        clUSI.Fields("agentid") = cboAgent.SelectedValue
        clUSI.Fields("allowcancelexpire") = chkAllowCancelExpire.Checked
        clUSI.Fields("within3days") = chkWithIn3Days.Checked
        clUSI.Fields("allcoverage") = chkAllCoverage.Checked
        clUSI.Fields("allowslush") = chkAllowSlush.Checked
        clUSI.Fields("cancel100") = chkCancel100.Checked
        clUSI.Fields("viewbreakdown") = chkViewBreakdown.Checked
        clUSI.Fields("AllowUndenyClaim") = chkAllowUndeny.Checked
        clUSI.Fields("allowclaimaudit") = chkClaimAudit.Checked
        clUSI.Fields("bypassmissinginfo") = chkBypassMissingInfo.Checked
        clUSI.Fields("allowtotalpayment") = chkTotalPayment.Checked
        clUSI.Fields("allow45days") = chkAllow45Days.Checked
        clUSI.Fields("allowazdenied") = chkAllowAZDenied.Checked
        clUSI.Fields("phonereports") = chkPhoneReports.Checked
        clUSI.Fields("ces") = chkCES.Checked
        If cboSubAgent.SelectedValue.Length = 0 Then
            clUSI.Fields("subagentid") = 0
        Else
            clUSI.Fields("subagentid") = cboSubAgent.SelectedValue
        End If
        If clUSI.RowCount = 0 Then
            clUSI.AddRow()
        End If
        clUSI.SaveDB()
        ClearFields()
        rgUser.Rebind()
        pnlDetail.Visible = False
        pnlSearch.Visible = True
    End Sub

    Private Sub GetUserInfo()
        Dim SQL As String
        Dim clU As New clsDBO
        SQL = "select * from userinfo "
        SQL = SQL + "where email = '" & txtEMail.Text & "' "
        SQL = SQL + "and password = '" & txtPassword.Text & "' "
        clU.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            hfUserDetailID.Value = clU.Fields("userid")
        End If
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        ClearFields()
        rgUser.Rebind()
        pnlDetail.Visible = False
        pnlSearch.Visible = True
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnToDoCreate_Click(sender As Object, e As EventArgs) Handles btnToDoCreate.Click
        Response.Redirect("~/users/todo.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnToDoReader_Click(sender As Object, e As EventArgs) Handles btnToDoReader.Click
        Response.Redirect("~/users/todoreader1.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub cboAgent_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboAgent.SelectedIndexChanged
        Dim li As New ListItem
        cboSubAgent.Items.Clear()
        li.Value = 0
        li.Text = "All"
        cboSubAgent.Items.Add(li)
        If cboAgent.SelectedValue = 0 Then
            Exit Sub
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from subagents "
        SQL = SQL + "where agentid = " & cboAgent.SelectedValue & " "
        SQL = SQL + "order by subagentname "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                li = New ListItem
                li.Value = clR.Fields("subagentid")
                li.Text = clR.Fields("subagentname")
                cboSubAgent.Items.Add(li)
            Next

        End If
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class