﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimPayment1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        dsPaid.ConnectionString = AppSettings("connstring")
        dsToBePaid.ConnectionString = AppSettings("connstring")
        dsWexMethod.ConnectionString = AppSettings("connstring")
        dsPayment.ConnectionString = AppSettings("connstring")
        dsClaimPayee.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlError.Visible = False
            pnlList.Visible = True
            pnlPaidDetail.Visible = False
            pnlToBePaidDetail.Visible = False
            pnlPaymentDetail.Visible = False
            pnlSearchClaimPayee.Visible = False
            pnlPaymentAdd.Visible = False
            trACH.Visible = False
            trCheck.Visible = False
            trCC.Visible = False
            trWex.Visible = False
            FillStatusAdd()
            FillPayMethodAdd()
            rgPaid.Rebind()
            rgToBePaid.Rebind()
            rgPayment.Rebind()
            FillPaymentStatus()
            If CheckLock() Then
                btnAddPayment.Visible = False
                btnProcessACH.Visible = False
                btnPaymentViewSave.Visible = False
                btnProcessACH.Visible = False
                btnProcessCC.Visible = False
                btnProcessCheck.Visible = False
                btnProcessWex.Visible = False
                btnSaveAdd.Visible = False
            Else
                btnAddPayment.Visible = True
                btnProcessACH.Visible = True
                btnPaymentViewSave.Visible = True
                btnProcessACH.Visible = True
                btnProcessCC.Visible = True
                btnProcessCheck.Visible = True
                btnProcessWex.Visible = True
                btnSaveAdd.Visible = True
            End If
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnAddPayment.Visible = False
                btnProcessACH.Visible = False
                btnPaymentViewSave.Visible = False
                btnProcessACH.Visible = False
                btnProcessCC.Visible = False
                btnProcessCheck.Visible = False
                btnProcessWex.Visible = False
                btnSaveAdd.Visible = False
            End If
        End If
    End Sub

    Private Function CheckLock() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckLock = False
        SQL = "Select claimid from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "And lockuserid <> " & hfUserID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            CheckLock = False
        Else
            CheckLock = True
        End If
    End Function

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillPaymentStatus()
        cboPaymentStatus.Items.Clear()
        cboPaymentStatus.Items.Add("")
        cboPaymentStatus.Items.Add("Paid")
        cboPaymentStatus.Items.Add("Payment Error")
        cboPaymentStatus.Items.Add("Transmitted To Wex")
        cboPaymentStatus.Items.Add("Void")
        cboPaymentMethod.Items.Add("Refund")
    End Sub

    Private Sub FillPayMethodAdd()
        cboPayMethodAdd.Items.Clear()
        cboPayMethodAdd.Items.Add("")
        cboPayMethodAdd.Items.Add("ACH")
        cboPayMethodAdd.Items.Add("Check")
        cboPayMethodAdd.Items.Add("Credit Card")
        cboPayMethodAdd.Items.Add("Wex")
    End Sub

    Private Sub FillStatusAdd()
        cboStatusAdd.Items.Add("Transmitted To Wex")
        cboStatusAdd.Items.Add("Paid")
        cboStatusAdd.Items.Add("Refund")
    End Sub

    Private Sub rgPaid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgPaid.SelectedIndexChanged
        pnlList.Visible = False
        pnlPaidDetail.Visible = True
        FillPaidDetail()
    End Sub

    Private Sub FillPaidDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select ClaimDetailID, cd.ClaimPayeeID,  cp.PayeeNo, "
        SQL = SQL + "cp.PayeeName,jobno  LossCode,DatePaid,PaidAmt from claimdetail cd "
        SQL = SQL + "left join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID "
        SQL = SQL + "where cd.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and cd.claimpayeeid = " & rgPaid.SelectedValue & " "
        SQL = SQL + "and claimdetailstatus = 'Paid' "
        rgPaidDetail.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        rgPaidDetail.DataBind()
    End Sub

    Private Sub btnPaidDetailClose_Click(sender As Object, e As EventArgs) Handles btnPaidDetailClose.Click
        pnlList.Visible = True
        pnlPaidDetail.Visible = False
    End Sub

    Private Sub rgToBePaid_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgToBePaid.SelectedIndexChanged
        hfClaimPayeeID.Value = rgToBePaid.SelectedValue
        pnlList.Visible = False
        pnlToBePaidDetail.Visible = True
        FillToBePaidDetail()
    End Sub

    Private Sub FillToBePaidDetail()
        GetPayeeInfo()
        GetClaimAmt()
        FillPaymentMethod()
        txtConfirmNo.Text = ""
        txtCheckNo.Text = ""
    End Sub

    Private Sub FillPaymentMethod()
        cboPaymentMethod.Items.Clear()
        cboPaymentMethod.Items.Add("")
        cboPaymentMethod.Items.Add("ACH")
        cboPaymentMethod.Items.Add("Check")
        cboPaymentMethod.Items.Add("Credit Card")
        cboPaymentMethod.Items.Add("Wex")
    End Sub

    Private Sub GetClaimAmt()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select sum(totalamt) as amt from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimpayeeid = " & hfClaimPayeeID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'authorized') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayAmt.Text = Format(CDbl(clR.Fields("amt")), "#,##0.00")
        End If
    End Sub

    Private Sub GetPayeeInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtClaimPayeeNo.Text = clR.Fields("payeeno")
            txtClaimPayeeName.Text = clR.Fields("payeename")
            CheckForInvoiceNo()
        End If
    End Sub

    Private Sub CheckForInvoiceNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claim c "
        SQL = SQL + "inner join servicecenter sc on c.servicecenterid = sc.servicecenterid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If txtClaimPayeeNo.Text.Trim = clR.Fields("servicecenterno") Then
                txtInvoiceNo.Text = clR.Fields("ronumber")
            End If
        End If
    End Sub

    Private Sub btnCloseToBePaidDetail_Click(sender As Object, e As EventArgs) Handles btnCloseToBePaidDetail.Click
        pnlList.Visible = True
        pnlToBePaidDetail.Visible = False
        trACH.Visible = False
        trCheck.Visible = False
        rgToBePaid.SelectedIndexes.Clear()
    End Sub

    Private Sub btnProcessACH_Click(sender As Object, e As EventArgs) Handles btnProcessACH.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trACH.Visible = False
        pnlList.Visible = True
        rgPaid.Rebind()
        rgToBePaid.Rebind()
        rgPayment.Rebind()
    End Sub

    Private Sub ProcessPayment()
        Dim SQL As String
        Dim SQLNote As String
        Dim clR As New clsDBO
        Dim clCPL As New clsDBO
        AddClaimPayment()
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and claimpayeeid = " & hfClaimPayeeID.Value & " "
        SQL = SQL + "and (claimdetailstatus = 'approved' "
        SQL = SQL + "or claimdetailstatus = 'authorized') "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from claimpaymentlink "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid") & " "
                SQL = SQL + "and claimpaymentid = " & hfClaimPaymentID.Value & " "
                clCPL.OpenDB(SQL, AppSettings("connstring"))
                If clCPL.RowCount = 0 Then
                    clCPL.NewRow()
                    clCPL.Fields("claimdetailid") = clR.Fields("claimdetailid")
                    clCPL.Fields("claimpaymentid") = hfClaimPaymentID.Value
                    clCPL.AddRow()
                    clCPL.SaveDB()
                End If
                SQL = "update claimdetail "
                If cboPaymentMethod.Text <> "Wex" Then
                    SQL = SQL + "set claimdetailstatus = 'Paid', "
                    SQL = SQL + "datepaid = '" & Today & "', "
                    SQL = SQL + "modby = " & hfUserID.Value & ", "
                    SQL = SQL + "moddate = '" & Today & "' "
                Else
                    SQL = SQL + "set claimdetailstatus = 'Transmitted', "
                    SQL = SQL + "modby = " & hfUserID.Value & ", "
                    SQL = SQL + "moddate = '" & Today & "' "
                End If
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid")
                clCPL.RunSQL(SQL, AppSettings("connstring"))
                SQLNote = "insert claimnote (claimid, claimnotetypeid, note, credate, creby, moddate, modby) "
                SQLNote = SQLNote + "values (" & hfClaimID.Value & "5,'Payment via " & cboPaymentMethod.Text & "', '" & Today & "'," & hfUserID.Value & ", '" & Today & "'," & hfUserID.Value & ")"
                clCPL.RunSQL(SQLNote, AppSettings("connstring"))
            Next
        End If
    End Sub

    Private Sub AddClaimPayment()
        Dim SQL As String
        Dim clCP As New clsDBO
        SQL = "select * from claimpayment "
        SQL = SQL + "where claimpaymentid = 0 "
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount = 0 Then
            clCP.NewRow()
            clCP.Fields("claimpayeeid") = hfClaimPayeeID.Value
            clCP.Fields("paymentamt") = txtPayAmt.Text
            If cboPaymentMethod.Text = "ACH" Then
                clCP.Fields("datepaid") = Today
                clCP.Fields("achinfo") = txtConfirmNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Check" Then
                clCP.Fields("datepaid") = Today
                clCP.Fields("checkno") = txtCheckNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Credit Card" Then
                clCP.Fields("datepaid") = Today
                clCP.Fields("checkno") = txtCCNo.Text
                clCP.Fields("status") = "Paid"
            End If
            If cboPaymentMethod.Text = "Wex" Then
                clCP.Fields("datetransmitted") = Today
                clCP.Fields("status") = "Transmitted To Wex"
                clCP.Fields("ccno") = hfWexCCno.Value
                clCP.Fields("wexcode") = hfWexCode.Value
                clCP.Fields("wexdeliverymethod") = cboWexMethod.SelectedText
                clCP.Fields("wexdeliveryaddress") = txtWexAddress.Text
                clCP.Fields("companyno") = hfCompanyNo.Value
            End If
            clCP.AddRow()
            clCP.SaveDB()
        End If

        SQL = "select max(claimpaymentid) as mCPI from claimpayment "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clCP.OpenDB(SQL, AppSettings("connstring"))
        If clCP.RowCount > 0 Then
            clCP.GetRow()
            hfClaimPaymentID.Value = clCP.Fields("mcpi")
        End If
    End Sub

    Private Sub cboPaymentMethod_TextChanged(sender As Object, e As EventArgs) Handles cboPaymentMethod.TextChanged
        trACH.Visible = False
        trWex.Visible = False
        trCheck.Visible = False
        trCC.Visible = False
        If cboPaymentMethod.Text = "ACH" Then
            trACH.Visible = True
        End If
        If cboPaymentMethod.Text = "Check" Then
            trCheck.Visible = True
        End If
        If cboPaymentMethod.Text = "Credit Card" Then
            trCC.Visible = True
        End If
        If cboPaymentMethod.Text = "Wex" Then
            trWex.Visible = True
        End If
    End Sub

    Private Sub btnProcessCheck_Click(sender As Object, e As EventArgs) Handles btnProcessCheck.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trCheck.Visible = False
        pnlList.Visible = True
        rgPaid.Rebind()
        rgToBePaid.Rebind()
        rgPayment.Rebind()
    End Sub

    Private Sub btnProcessCC_Click(sender As Object, e As EventArgs) Handles btnProcessCC.Click
        ProcessPayment()
        pnlToBePaidDetail.Visible = False
        trCC.Visible = False
        pnlList.Visible = True
        rgPaid.Rebind()
        rgToBePaid.Rebind()
        rgPayment.Rebind()
    End Sub

    Private Sub btnProcessWex_Click(sender As Object, e As EventArgs) Handles btnProcessWex.Click
        Dim SQL As String
        Dim clR As New clsDBO
        txtWexAddress.Text = txtWexAddress.Text.Trim
        If cboWexMethod.SelectMethod = "Fax" Then
            txtWexAddress.Text = txtWexAddress.Text.Replace("(", "")
            txtWexAddress.Text = txtWexAddress.Text.Replace(")", "")
            txtWexAddress.Text = txtWexAddress.Text.Replace(" ", "")
            txtWexAddress.Text = txtWexAddress.Text.Replace("-", "")
            txtWexAddress.Text = txtWexAddress.Text.Replace(".", "")
        End If
        ProcessWexDLL()
        rgPaid.Rebind()
        rgToBePaid.Rebind()
        rgPayment.Rebind()
        pnlToBePaidDetail.Visible = False
        trWex.Visible = False
        pnlList.Visible = True
        If cboWexMethod.SelectedValue = "EMail" Then
            SQL = "Update servicecenter "
            SQL = SQL + "set email = '" & txtWexAddress.Text & "' "
            SQL = SQL + "where servicecenterno = '" & txtClaimPayeeNo.Text.Trim & "' "
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
        If cboWexMethod.SelectedValue = "Fax" Then
            SQL = "Update servicecenter "
            SQL = SQL + "set fax = '" & txtWexAddress.Text & "' "
            SQL = SQL + "where servicecenterno = '" & txtClaimPayeeNo.Text.Trim & "' "
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If
    End Sub

    Private Sub ProcessWexDLL()
        Dim clCWP As New clsDBO
        Dim clR As New clsDBO

        GetClaimNo()
        If AppSettings("connstring") = "server=198.143.98.122;database=veritastest;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E" Then
            Exit Sub
        Else
            pnlError.Visible = False
            If CheckPayment() Then
                pnlError.Visible = True
                lblError.Text = "Already paid to " & txtWexAddress.Text & " for " & txtPayAmt.Text
                Exit Sub
            End If
            GetCompanyNo()
            Dim SQL As String
            Dim clWA As New clsDBO
            SQL = "select * from wexapi "
            SQL = SQL + "where companyno = '" & hfCompanyNo.Value & "' "
            clWA.OpenDB(SQL, AppSettings("connstring"))
            If clWA.RowCount > 0 Then
                clWA.GetRow()
                SQL = "select * from claimwexpayment "
                SQL = SQL + "where claimwexpaymentid = 0 "
                clCWP.OpenDB(SQL, AppSettings("connstring"))
                If clCWP.RowCount = 0 Then
                    clCWP.NewRow()
                    clCWP.Fields("claimno") = hfClaimNo.Value
                    clCWP.Fields("claimid") = hfClaimID.Value
                    clCWP.Fields("rono") = txtInvoiceNo.Text
                    clCWP.Fields("orggroup") = clWA.Fields("OrgGroup")
                    clCWP.Fields("username") = clWA.Fields("UserName")
                    clCWP.Fields("password") = clWA.Fields("Password")
                    clCWP.Fields("currency") = clWA.Fields("currency")
                    clCWP.Fields("wexaddress") = txtWexAddress.Text
                    clCWP.Fields("wexmethod") = cboWexMethod.SelectedValue
                    clCWP.Fields("bankno") = clWA.Fields("bankno")
                    clCWP.Fields("companyno") = hfCompanyNo.Value
                    clCWP.Fields("paymentamt") = txtPayAmt.Text
                    clCWP.Fields("contractno") = hfContractNo.Value
                    clCWP.Fields("customername") = hfCustomerName.Value
                    clCWP.Fields("Claimpayeename") = txtClaimPayeeName.Text
                    clCWP.Fields("payeecontact") = hfPayeeContact.Value
                    clCWP.Fields("vin") = hfVIN.Value
                    clCWP.AddRow()
                    clCWP.SaveDB()
                    SQL = "select max(claimwexpaymentid) as MCWP from claimwexpayment "
                    SQL = SQL + "where claimid = " & hfClaimID.Value
                    clCWP.OpenDB(SQL, AppSettings("connstring"))
                    If clCWP.RowCount > 0 Then
                        clCWP.GetRow()
                        If Not AppSettings("connstring").ToLower.Contains("test") Then
                            Dim pr As New Process
                            pr.StartInfo.FileName = "C:\ProcessProgram\WexPayment\WexPayment.exe"
                            pr.StartInfo.Arguments = clCWP.Fields("mcwp")
                            pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
                            pr.Start()
                            pr.WaitForExit()
                            pr.Close()
                            SQL = "select * from claimwexpayment "
                            SQL = SQL + "where claimwexpaymentid = " & clCWP.Fields("mcwp")
                            clR.OpenDB(SQL, AppSettings("connstring"))
                            If clR.RowCount > 0 Then
                                clR.GetRow()
                                If clR.Fields("ReasonCode").ToLower = "success" Then
                                    hfWexCCno.Value = clR.Fields("wexccno").Substring(0, 4)
                                    hfWexCode.Value = clR.Fields("wexcode")
                                    ProcessPayment()
                                    pnlToBePaidDetail.Visible = False
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function CheckPayment() As Boolean
        CheckPayment = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimwexpayment "
        SQL = SQL + "where  wexaddress = '" & txtWexAddress.Text & "' "
        SQL = SQL + "and paymentamt = " & CDbl(txtPayAmt.Text) & " "
        SQL = SQL + "and sent >= '" & DateAdd(DateInterval.Day, -5, Today) & "' "
        SQL = SQL + "and claimno = '" & hfClaimNo.Value & "' "
        SQL = SQL + "and voiddate is null "
        SQL = SQL + "and not wexcode is null "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckPayment = True
        End If
    End Function

    Private Sub GetClaimNo()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claim "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimNo.Value = clR.Fields("claimno")
            hfRONo.Value = clR.Fields("RONumber")
            hfContractID.Value = clR.Fields("contractid")
            OpenContract()
            hfPayeeContact.Value = clR.Fields("sccontactinfo")
        End If
    End Sub

    Private Sub OpenContract()
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfContractNo.Value = clR.Fields("contractno")
            hfCustomerName.Value = clR.Fields("fname") & " " & clR.Fields("lname")
            hfVIN.Value = clR.Fields("vin")
        End If
    End Sub

    Private Sub GetCompanyNo()
        Dim clCL As New clsDBO
        Dim SQL As String
        SQL = "select * from claim cl "
        SQL = SQL + "inner join contract c on c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clCL.OpenDB(SQL, AppSettings("connstring"))
        If clCL.RowCount > 0 Then
            clCL.GetRow()
            hfCompanyNo.Value = "0000976"
            If clCL.Fields("clipid") = 1 Or clCL.Fields("clipid") = 2 Then
                hfCompanyNo.Value = "0000978"
            End If
            If clCL.Fields("clipid") = 3 Or clCL.Fields("clipid") = 4 Or clCL.Fields("clipid") = 11 Then
                If clCL.Fields("contractno").Substring(0, 3) = "CHJ" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "DRV" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAC" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAD" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RAN" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RDI" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "REP" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSA" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSD" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "RSW" Then
                    hfCompanyNo.Value = "0000978"
                ElseIf clCL.Fields("Contractno").Substring(0, 3) = "VEL" Then
                    hfCompanyNo.Value = "0000978"
                Else
                    hfCompanyNo.Value = "0000976"
                End If
            End If
            If clCL.Fields("inscarrierid") = 4 Then
                If isfl Then
                    hfCompanyNo.Value = "0000977"
                Else
                    hfCompanyNo.Value = "0000976"
                End If
            End If
            If clCL.Fields("inscarrierid") = 5 Then
                If isfl Then
                    hfCompanyNo.Value = "0000977"
                Else
                    hfCompanyNo.Value = "0000976"
                End If
            End If
            If clCL.Fields("clipid") = 5 Or clCL.Fields("clipid") = 6 Or clCL.Fields("clipid") = 14 Then
                hfCompanyNo.Value = "0000976"
            End If
            If clCL.Fields("clipid") = 7 Or clCL.Fields("clipid") = 8 Or clCL.Fields("clipid") = 13 Then
                hfCompanyNo.Value = "0000976"
            End If
            If clCL.Fields("clipid") = 9 Or clCL.Fields("clipid") = 10 Or clCL.Fields("clipid") = 12 Then
                hfCompanyNo.Value = "0000977"
            End If
            If clCL.Fields("clipid") = 11 Then
                hfCompanyNo.Value = "0000978"
            End If
            If clCL.Fields("clipid") = 0 Then
                If clCL.Fields("contractno").Substring(0, 3) = "VA1" Then
                    hfCompanyNo.Value = "0000976"
                End If
                If clCL.Fields("contractno").Substring(0, 3) = "VAO" Then
                    hfCompanyNo.Value = "0000976"
                End If
            End If
        End If
    End Sub

    Private Function isFL() As Boolean
        isFL = False
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select c.State from claim cl inner join contract c on c.contractid = cl.contractid "
        SQL = SQL + "where cl.ClaimID = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("state").ToUpper = "FL" Then
                isFL = True
            End If
        End If
    End Function

    Private Sub rgPayment_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgPayment.SelectedIndexChanged
        pnlList.Visible = False
        pnlPaymentDetail.Visible = True
        FillPaymentDetail()
    End Sub

    Private Sub FillPaymentDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        txtPaymentAmt.Text = ""
        txtPayeeName.Text = ""
        txtPayeeNo.Text = ""
        txtDatePaid.Text = ""
        txtDateTransmitted.Text = ""
        txtACHInfo.Text = ""
        txtCheckNoView.Text = ""
        cboPaymentStatus.Text = ""
        txtCCNoView.Text = ""
        txtWexMethod.Text = ""
        txtWexAddressView.Text = ""
        txtWexCode.Text = ""
        txtCompanyNo.Text = ""
        hfClaimPaymentID.Value = rgPayment.SelectedValue
        SQL = "select * from claimpayment "
        SQL = SQL + "where claimpaymentid = " & rgPayment.SelectedValue
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
            GetPayeeInfo2()
            txtPaymentAmt.Text = clR.Fields("paymentamt")
            txtPayMethod.Text = clR.Fields("paymethod")
            If clR.Fields("datetransmitted").Length > 0 Then
                txtDateTransmitted.Text = Format(CDate(clR.Fields("datetransmitted")), "M/d/yyyy")
            End If
            If clR.Fields("datepaid").Length > 0 Then
                txtDatePaid.Text = Format(CDate(clR.Fields("datepaid")), "M/d/yyyy")
            End If
            txtACHInfo.Text = clR.Fields("achinfo")
            txtCheckNoView.Text = clR.Fields("checkno")
            txtCCNoView.Text = clR.Fields("ccno")
            txtWexAddressView.Text = clR.Fields("wexdeliveryaddress")
            txtWexMethod.Text = clR.Fields("wexdeliverymethod")
            txtWexCode.Text = clR.Fields("wexcode")
            cboPaymentStatus.Text = clR.Fields("status")
            txtCompanyNo.Text = clR.Fields("companyno")
        End If
    End Sub

    Private Sub GetPayeeInfo2()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayeeNo.Text = clR.Fields("payeeno")
            txtPayeeName.Text = clR.Fields("payeename")
        End If
    End Sub

    Private Sub btnPaymentViewClose_Click(sender As Object, e As EventArgs) Handles btnPaymentViewClose.Click
        pnlList.Visible = True
        pnlPaymentDetail.Visible = False
        rgPayment.SelectedIndexes.Clear()
    End Sub

    Private Sub btnSeekPayee_Click(sender As Object, e As EventArgs) Handles btnSeekPayee.Click
        pnlSearchClaimPayee.Visible = True
        pnlPaymentAdd.Visible = False
    End Sub

    Private Sub btnAddPayment_Click(sender As Object, e As EventArgs) Handles btnAddPayment.Click
        pnlList.Visible = False
        pnlPaymentAdd.Visible = True
    End Sub

    Private Sub btnCloseAdd_Click(sender As Object, e As EventArgs) Handles btnCloseAdd.Click
        pnlPaymentAdd.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSaveAdd_Click(sender As Object, e As EventArgs) Handles btnSaveAdd.Click
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clP As New clsDBO
        SQL = "select * from claimpayment "
        SQL = SQL + "where claimpaymentid = 0 "
        clP.OpenDB(SQL, AppSettings("connstring"))
        If clP.RowCount = 0 Then
            clP.NewRow()
            clP.Fields("claimpayeeid") = hfClaimPayeeID.Value
            clP.Fields("paymentamt") = txtPaymentAmtAdd.Text
            If Not rdpDateTransmittedAdd.SelectedDate Is Nothing Then
                clP.Fields("datetransmitted") = rdpDateTransmittedAdd.SelectedDate
            End If
            clP.Fields("datepaid") = rdpDatePaidAdd.SelectedDate
            clP.Fields("paymethod") = cboPayMethodAdd.SelectedValue
            clP.Fields("achinfo") = txtACHInfo.Text
            clP.Fields("checkno") = txtCheckNo.Text
            clP.Fields("ccno") = txtCCNo.Text
            clP.Fields("wexdeliverymethod") = txtWexDeliveryMethodAdd.Text
            clP.Fields("wexdeliveryaddress") = txtWexDeliveryAddressAdd.Text
            clP.Fields("WexCode") = txtWexCodeAdd.Text
            clP.Fields("status") = cboStatusAdd.SelectedValue
            clP.Fields("companyno") = txtCompanyNoAdd.Text
            clP.AddRow()
            clP.SaveDB()
        End If
        SQL = "select max(claimpaymentid) as MaxClaimPayID from claimpayment "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clP.OpenDB(SQL, AppSettings("connstring"))
        If clP.RowCount > 0 Then
            clP.GetRow()
            hfClaimPaymentID.Value = clP.Fields("maxclaimpayid")
        End If
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value & " "
        SQL = SQL + "and claimid = " & hfClaimID.Value
        clP.OpenDB(SQL, AppSettings("connstring"))
        If clP.RowCount > 0 Then
            For cnt = 0 To clP.RowCount - 1
                clP.GetRowNo(cnt)
                SQL = "select * from claimpaymentlink "
                SQL = SQL + "where claimdetailid = " & clP.Fields("claimdetailid") & " "
                SQL = SQL + "and claimpaymentid = " & hfClaimPaymentID.Value
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount = 0 Then
                    clR.NewRow()
                    clR.Fields("Claimdetailid") = clP.Fields("claimdetailid")
                    clR.Fields("claimpaymentid") = hfClaimPaymentID.Value
                    clR.AddRow()
                    clR.SaveDB()
                End If
            Next
        End If
        pnlList.Visible = True
        pnlPaymentAdd.Visible = False
        rgPayment.Rebind()
    End Sub

    Private Sub rgClaimPayee_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimPayee.SelectedIndexChanged
        hfClaimPayeeID.Value = rgClaimPayee.SelectedValue
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayee "
        SQL = SQL + "where claimpayeeid = " & hfClaimPayeeID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtPayeeNoAdd.Text = clR.Fields("payeeno")
            txtPayeeNameAdd.Text = clR.Fields("payeename")
            hfClaimPayeeID.Value = clR.Fields("claimpayeeid")
        End If
        pnlSearchClaimPayee.Visible = False
        pnlPaymentAdd.Visible = True
    End Sub

    Private Sub btnPaymentViewSave_Click(sender As Object, e As EventArgs) Handles btnPaymentViewSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimpayment "
        SQL = SQL + "where claimpaymentid = " & hfClaimPaymentID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("achinfo") = txtACHInfo.Text
            clR.Fields("checkno") = txtCheckNoView.Text
            clR.Fields("ccno") = txtCCNoView.Text
            clR.Fields("status") = cboPaymentStatus.Text
            clR.SaveDB()
        End If
        If cboPaymentStatus.Text = "Void" Then
            ProcessVoid()
        End If
        pnlList.Visible = True
        pnlPaymentDetail.Visible = False
        rgPayment.Rebind()
        rgToBePaid.Rebind()
    End Sub

    Private Sub ProcessVoid()
        Dim SQL As String
        Dim clCWP As New clsDBO
        Dim clCP As New clsDBO
        Dim clR As New clsDBO
        SQL = "select * from claimwexpayment "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and wexcode = '" & txtWexCode.Text & "' "
        clCWP.OpenDB(SQL, AppSettings("connstring"))
        If clCWP.RowCount > 0 Then
            clCWP.GetRow()
            Dim pr As New Process
            pr.StartInfo.FileName = "C:\ProcessProgram\WexVoid\WexVoid.exe"
            pr.StartInfo.Arguments = clCWP.Fields("claimwexpaymentid")
            pr.StartInfo.WindowStyle = ProcessWindowStyle.Hidden
            pr.Start()
            pr.WaitForExit()
            pr.Close()
        End If
        clCWP.OpenDB(SQL, AppSettings("connstring"))
        If clCWP.RowCount > 0 Then
            clCWP.GetRow()
            If clCWP.Fields("voiddate").Length > 0 Then
                SQL = "update claimdetail "
                SQL = SQL + "set claimdetailstatus = 'Authorized' "
                SQL = SQL + "where claimdetailid in ("
                SQL = SQL + "select cd.claimdetailid from claimpayment cp "
                SQL = SQL + "inner join ClaimPaymentLink cpl on cp.ClaimPaymentID = cpl.ClaimPaymentID "
                SQL = SQL + "inner join claimdetail cd on cd.ClaimDetailID = cpl.ClaimDetailID "
                SQL = SQL + "where cd.claimid = " & hfClaimID.Value & " "
                SQL = SQL + "and wexcode = '" & txtWexCode.Text & "') "
                clR.RunSQL(SQL, AppSettings("connstring"))
            End If
        End If


    End Sub

    Private Sub cboWexMethod_SelectedIndexChanged(sender As Object, e As DropDownListEventArgs) Handles cboWexMethod.SelectedIndexChanged
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from servicecenter sc "
        SQL = SQL + "where servicecenterno = '" & txtClaimPayeeNo.Text.Trim & "' "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If cboWexMethod.SelectedValue = "EMail" Then
                txtWexAddress.Text = clR.Fields("email").Trim
            End If
            If cboWexMethod.SelectedValue = "Fax" Then
                txtWexAddress.Text = clR.Fields("fax").Trim
                txtWexAddress.Text = txtWexAddress.Text.Replace("(", "")
                txtWexAddress.Text = txtWexAddress.Text.Replace(")", "")
                txtWexAddress.Text = txtWexAddress.Text.Replace(" ", "")
                txtWexAddress.Text = txtWexAddress.Text.Replace("-", "")
                txtWexAddress.Text = txtWexAddress.Text.Replace(".", "")
            End If
        End If
    End Sub
End Class