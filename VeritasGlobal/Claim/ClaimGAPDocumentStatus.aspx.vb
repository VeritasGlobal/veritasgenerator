﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimGAPDocumentStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        If Not IsPostBack Then
            lblGenerateClaimNotice.Visible = False
            lblStatusLetter.Visible = False
            lblPaymentLetter.Visible = False
            lblMissedOptions.Visible = False
            lblGenerate60DayLetter.Visible = False
            lblGapNoGapDueLetter.Visible = False
            lblDenialLetter.Visible = False
            GetServerInfo()
            GetInfo()
        End If
    End Sub

    Private Sub GetInfo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocstatus "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            chk60DayLetter.Checked = CBool(clR.Fields("gap60dayletter"))
            chkDenialLetter.Checked = CBool(clR.Fields("gapdenielletter"))
            chkGAPClaimNotice.Checked = CBool(clR.Fields("gapclaimnotice"))
            chkGAPNoGAPDueLetter.Checked = CBool(clR.Fields("gapnogapdueletter"))
            chkMissedOptions.Checked = CBool(clR.Fields("gapmissedoptions"))
            chkPaymentHistory.Checked = CBool(clR.Fields("paymenthistory"))
            chkPaymentLetter.Checked = CBool(clR.Fields("gappaymentletter"))
            chkPoliceFireReport.Checked = CBool(clR.Fields("policefirereport"))
            chkProofInsPayments.Checked = CBool(clR.Fields("ProofInsPayments"))
            chkRISC.Checked = CBool(clR.Fields("risc"))
            chkStatusLetter.Checked = CBool(clR.Fields("gapstatusletter"))
            chkTotalLossInsSettlementLetter.Checked = CBool(clR.Fields("TotalLossSettlement"))
            chkTotalLossInsValuation.Checked = CBool(clR.Fields("TotalLossValuation"))
        Else
            chk60DayLetter.Checked = False
            chkDenialLetter.Checked = False
            chkGAPClaimNotice.Checked = False
            chkGAPNoGAPDueLetter.Checked = False
            chkMissedOptions.Checked = False
            chkPaymentHistory.Checked = False
            chkPaymentLetter.Checked = False
            chkPoliceFireReport.Checked = False
            chkProofInsPayments.Checked = False
            chkRISC.Checked = False
            chkStatusLetter.Checked = False
            chkTotalLossInsSettlementLetter.Checked = False
            chkTotalLossInsValuation.Checked = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocstatus "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
        End If
        clR.Fields("risc") = chkRISC.Checked
        clR.Fields("paymenthistory") = chkPaymentHistory.Checked
        clR.Fields("totallossvaluation") = chkTotalLossInsValuation.Checked
        clR.Fields("totallosssettlement") = chkTotalLossInsSettlementLetter.Checked
        clR.Fields("policefirereport") = chkPoliceFireReport.Checked
        clR.Fields("proofinspayments") = chkProofInsPayments.Checked
        clR.Fields("gapclaimnotice") = chkGAPClaimNotice.Checked
        clR.Fields("gap60dayletter") = chk60DayLetter.Checked
        clR.Fields("gapdenielletter") = chkDenialLetter.Checked
        clR.Fields("gapmissedoptions") = chkMissedOptions.Checked
        clR.Fields("gapnogapdueletter") = chkGAPNoGAPDueLetter.Checked
        clR.Fields("gappaymentletter") = chkPaymentLetter.Checked
        clR.Fields("gapstatusletter") = chkStatusLetter.Checked
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub

    Private Sub btnDenialLetter_Click(sender As Object, e As EventArgs) Handles btnDenialLetter.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",9)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblDenialLetter.Text = "Document is being created. You will notified by e-mail when complete."
        lblDenialLetter.Visible = True
    End Sub

    Private Sub btnGAPNoGAPDueLetter_Click(sender As Object, e As EventArgs) Handles btnGAPNoGAPDueLetter.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",11)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblGapNoGapDueLetter.Text = "Document is being created. You will notified by e-mail when complete."
        lblGapNoGapDueLetter.Visible = True
    End Sub

    Private Sub btnGenerate60DayLetter_Click(sender As Object, e As EventArgs) Handles btnGenerate60DayLetter.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",8)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblGenerate60DayLetter.Text = "Document is being created. You will notified by e-mail when complete."
        lblGenerate60DayLetter.Visible = True
    End Sub

    Private Sub btnGenerateClaimNotice_Click(sender As Object, e As EventArgs) Handles btnGenerateClaimNotice.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",7)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblGenerateClaimNotice.Text = "Document is being created. You will notified by e-mail when complete."
        lblGenerateClaimNotice.Visible = True
    End Sub

    Private Sub btnMissedOptions_Click(sender As Object, e As EventArgs) Handles btnMissedOptions.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",10)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblMissedOptions.Text = "Document is being created. You will notified by e-mail when complete."
        lblMissedOptions.Visible = True
    End Sub

    Private Sub btnPaymentLetter_Click(sender As Object, e As EventArgs) Handles btnPaymentLetter.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",12)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblPaymentLetter.Text = "Document is being created. You will notified by e-mail when complete."
        lblPaymentLetter.Visible = True
    End Sub

    Private Sub btnStatusLetter_Click(sender As Object, e As EventArgs) Handles btnStatusLetter.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "insert into ClaimGapDocumentRequest "
        SQL = SQL + "(userid, claimgapid, claimdoctypeid) "
        SQL = SQL + "values (" & hfUserID.Value & ", "
        SQL = SQL + hfClaimID.Value & ",13)"
        clR.RunSQL(SQL, AppSettings("connstring"))
        lblStatusLetter.Text = "Document is being created. You will notified by e-mail when complete."
        lblStatusLetter.Visible = True
    End Sub
End Class