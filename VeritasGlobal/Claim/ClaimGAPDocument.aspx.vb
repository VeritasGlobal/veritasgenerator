﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI
Imports System.IO

Public Class ClaimGAPDocument
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        dsDocs.ConnectionString = AppSettings("connstring")
        dsDocType.ConnectionString = AppSettings("connstring")
        dsDocType2.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlList.Visible = True
            pnlAdd.Visible = False
            pnlDetail.Visible = False

            rgClaimDocument.Rebind()

        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnAdd_Click(sender As Object, e As EventArgs) Handles btnAdd.Click
        pnlAdd.Visible = True
        pnlList.Visible = False
        txtDocName.Text = ""
        txtDocDesc.Text = ""

    End Sub

    Private Sub btnCloseAdd_Click(sender As Object, e As EventArgs) Handles btnCloseAdd.Click
        pnlAdd.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnUpload_Click(sender As Object, e As EventArgs) Handles btnUpload.Click
        Dim sDocLink As String
        Dim SQL As String
        Dim clR As New clsDBO
        GetClaimNo()
        Dim folderPath As String = Server.MapPath("~") & "\documents\gap\" & hfClaimNo.Value & "_" & RadUpload1.UploadedFiles(0).FileName

        sDocLink = "~/documents/gap/" & hfClaimNo.Value & "_" & RadUpload1.UploadedFiles(0).FileName
        RadUpload1.UploadedFiles(0).SaveAs(folderPath)

        SQL = "select * from claimgapdocument "
        SQL = SQL + "where claimgapid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        clR.NewRow()
        clR.Fields("claimgapid") = hfClaimID.Value
        clR.Fields("documentname") = txtDocName.Text
        clR.Fields("documentdesc") = txtDocDesc.Text
        clR.Fields("documentlink") = sDocLink
        clR.Fields("claimgapdoctypeid") = cboDocType2.SelectedValue
        clR.Fields("creby") = hfUserID.Value
        clR.Fields("credate") = Now
        clR.Fields("moddate") = Now
        clR.Fields("modby") = hfUserID.Value
        clR.AddRow()
        clR.SaveDB()
        pnlAdd.Visible = False
        pnlList.Visible = True
        rgClaimDocument.Rebind()

    End Sub

    Private Sub GetClaimNo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimno from claimgap "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            hfClaimNo.Value = clR.Fields("claimno")
        End If
    End Sub

    Private Sub rgClaimDocument_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgClaimDocument.SelectedIndexChanged
        pnlList.Visible = False
        pnlDetail.Visible = True
        hfDocID.Value = rgClaimDocument.SelectedValue
        FillDetail()
    End Sub

    Private Sub FillDetail()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocument "
        SQL = SQL + "where claimgapdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtCreBy.Text = GetUserInfo(clR.Fields("creby"))
            txtCreDate.Text = clR.Fields("credate")
            txtDescDetail.Text = clR.Fields("documentdesc")
            If clR.Fields("modby").Length > 0 Then
                txtModBy.Text = GetUserInfo(clR.Fields("modby"))
            End If
            txtTitleDetail.Text = clR.Fields("documentname")
            txtModDate.Text = clR.Fields("moddate")
            cboDocType.SelectedValue = clR.Fields("claimgapdoctypeid")
        End If
    End Sub

    Private Sub btnClose_Click(sender As Object, e As EventArgs) Handles btnClose.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnDelete_Click(sender As Object, e As EventArgs) Handles btnDelete.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimgapdocument "
        SQL = SQL + "set deleted = 1 "
        SQL = SQL + "where claimgapdocumentid = " & hfDocID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgClaimDocument.Rebind()
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapdocument "
        SQL = SQL + "where claimgapdocumentid = " & hfDocID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If cboDocType.SelectedValue <> clR.Fields("claimgapdoctypeid") Then
                ChangeDocStatus(clR.Fields("claimgapdoctypeid"), cboDocType.SelectedValue)
            End If
            clR.Fields("claimgapdoctypeid") = cboDocType.SelectedValue
            clR.Fields("documentname") = txtTitleDetail.Text
            clR.Fields("documentdesc") = txtDescDetail.Text
            clR.Fields("modby") = hfUserID.Value
            clR.Fields("moddate") = Now
            clR.SaveDB()
        End If
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgClaimDocument.Rebind()
    End Sub

    Private Sub ChangeDocStatus(xOld As Long, xNew As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimgapdocstatus "
        If xOld = 1 Then
            SQL = SQL + "set risc = 0 "
        End If
        If xOld = 2 Then
            SQL = SQL + "set paymenthistory = 0 "
        End If
        If xOld = 3 Then
            SQL = SQL + "set TotalLossValuation = 0 "
        End If
        If xOld = 4 Then
            SQL = SQL + "set TotalLossSettlement = 0 "
        End If
        If xOld = 5 Then
            SQL = SQL + "set PoliceFireReport = 0 "
        End If
        If xOld = 6 Then
            SQL = SQL + "set ProofInsPayments = 0 "
        End If
        If xOld = 7 Then
            SQL = SQL + "set GAPClaimNotice = 0 "
        End If
        If xOld = 8 Then
            SQL = SQL + "set GAP60DayLetter = 0 "
        End If
        If xOld = 9 Then
            SQL = SQL + "set GAPDenielLetter = 0 "
        End If
        If xOld = 10 Then
            SQL = SQL + "set GAPMissedOptions = 0 "
        End If
        If xOld = 11 Then
            SQL = SQL + "set GAPNoGAPDueLetter = 0 "
        End If
        If xOld = 12 Then
            SQL = SQL + "set GAPPaymentLetter = 0 "
        End If
        If xOld = 13 Then
            SQL = SQL + "set GAPStatusLetter = 0 "
        End If
        SQL = SQL + "where claimgaPid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "update claimgapdocstatus "
        If xNew = 1 Then
            SQL = SQL + "set risc = 1 "
        End If
        If xNew = 2 Then
            SQL = SQL + "set paymenthistory = 1 "
        End If
        If xNew = 3 Then
            SQL = SQL + "set TotalLossValuation = 1 "
        End If
        If xNew = 4 Then
            SQL = SQL + "set TotalLossSettlement = 1 "
        End If
        If xNew = 5 Then
            SQL = SQL + "set PoliceFireReport = 1 "
        End If
        If xNew = 6 Then
            SQL = SQL + "set ProofInsPayments = 1 "
        End If
        If xNew = 7 Then
            SQL = SQL + "set GAPClaimNotice = 1 "
        End If
        If xNew = 8 Then
            SQL = SQL + "set GAP60DayLetter = 1 "
        End If
        If xNew = 9 Then
            SQL = SQL + "set GAPDenielLetter = 1 "
        End If
        If xNew = 10 Then
            SQL = SQL + "set GAPMissedOptions = 1 "
        End If
        If xNew = 11 Then
            SQL = SQL + "set GAPNoGAPDueLetter = 1 "
        End If
        If xNew = 12 Then
            SQL = SQL + "set GAPPaymentLetter = 1 "
        End If
        If xNew = 13 Then
            SQL = SQL + "set GAPStatusLetter = 1 "
        End If
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))

    End Sub
End Class