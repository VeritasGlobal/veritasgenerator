﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ClaimTicketMessage
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsNote.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            pnlList.Visible = True
            pnlDetail.Visible = False
            FillClaimCombo()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
            FillClaimCombo()
        End If
    End Sub

    Private Sub FillClaimCombo()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select claimid, claimno from claim "
        SQL = SQL + "where assignedto = " & hfUserID.Value & " "
        SQL = SQL + "and claimid in (select claimid from veritasclaimticket.dbo.note) "
        cboClaim.DataSource = clR.GetData(SQL, AppSettings("connstring"))
        cboClaim.DataBind()
        hfClaimID.Value = cboClaim.SelectedValue
        rgNote.Rebind()
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub rgNote_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rgNote.SelectedIndexChanged
        Dim SQL As String
        Dim clR As New clsDBO
        btnSave.Visible = False
        txtNote.Enabled = False
        pnlDetail.Visible = True
        pnlList.Visible = False
        SQL = "select * from veritasclaimticket.dbo.note "
        SQL = SQL + "where noteid = " & rgNote.SelectedValue
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtNote.Content = clR.Fields("note")
            txtCreBy.Text = clR.Fields("creby")
            txtCreDate.Text = clR.Fields("credate")
            txtResponseBy.Text = clR.Fields("responseby")
            txtResponseDate.Text = clR.Fields("responsedate")
        End If
    End Sub

    Private Sub btnAddNote_Click(sender As Object, e As EventArgs) Handles btnAddNote.Click
        pnlDetail.Visible = True
        pnlList.Visible = False
        btnSave.Visible = True
        txtNote.Enabled = True
        txtNote.Content = ""
        txtCreBy.Text = ""
        txtCreDate.Text = ""
        txtResponseBy.Text = ""
        txtResponseDate.Text = ""
    End Sub

    Private Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        pnlDetail.Visible = False
        pnlList.Visible = True
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from veritasclaimticket.dbo.note "
        SQL = SQL + "where noteid = 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimid") = hfClaimID.Value
            clR.Fields("notetype") = "Response"
            clR.Fields("note") = txtNote.Content
            clR.Fields("notetext") = txtNote.Text
            clR.Fields("responseby") = CalcUserName()
            clR.Fields("responsedate") = Today
            clR.AddRow()
            clR.SaveDB()
        End If
        SQL = "update veritasclaimticket.dbo.ticket "
        SQL = SQL + "set statusid = 1 "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        SQL = SQL + "and statusid <> 4 "
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "update veritasclaimticket.dbo.ticket "
        SQL = SQL + "set statusid = 2 "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and responseid <> 1 "
        SQL = SQL + "and statusid <> 4 "
        clR.RunSQL(SQL, AppSettings("connstring"))
        pnlDetail.Visible = False
        pnlList.Visible = True
        rgNote.Rebind()
    End Sub

    Private Sub cboClaim_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cboClaim.SelectedIndexChanged
        hfClaimID.Value = cboClaim.SelectedValue
        rgNote.Rebind()
    End Sub

    Private Function CalcUserName() As String
        CalcUserName = ""
        Dim SQL As String
        Dim clU As New clsDBO
        CalcUserName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clU.OpenDB(SQL, AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            CalcUserName = clU.Fields("username")
        End If

    End Function


End Class