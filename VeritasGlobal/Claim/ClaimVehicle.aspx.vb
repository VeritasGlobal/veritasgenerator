﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimVehicle1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("ContractID")
        pvMoxy.ContentUrl = "ClaimVINMoxy.aspx?ContractID=" & hfContractID.Value
        pvBasicData.ContentUrl = "ClaimVINBasicData.aspx?ContractID=" & hfContractID.Value
        pvEngine.ContentUrl = "ClaimVINEngine.aspx?ContractID=" & hfContractID.Value
        pvTransmission.ContentUrl = "ClaimVINTransmission.aspx?ContractID=" & hfContractID.Value
        pvWarranty.ContentUrl = "ClaimVINWarranty.aspx?ContractID=" & hfContractID.Value
        If Not IsPostBack Then
            pvMoxy.Selected = True
            tsClaim.SelectedIndex = 0
            FillVehicleInfo()
        End If
    End Sub

    Private Sub FillVehicleInfo()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim clV As New VeritasGlobalTools.clsVIN
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            clV.Contract = clC.Fields("contractno")
            clV.VIN = clC.Fields("vin")
            clV.ProcessVIN()
            hfVIN.Value = clC.Fields("vin")
        End If
    End Sub

    Private Sub tsClaim_TabClick(sender As Object, e As RadTabStripEventArgs) Handles tsClaim.TabClick
        If tsClaim.SelectedTab.Value = "Moxy" Then
            pvMoxy.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Basic" Then
            pvBasicData.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Engine" Then
            pvEngine.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Transmission" Then
            pvTransmission.Selected = True
        End If
        If tsClaim.SelectedTab.Value = "Warranty" Then
            pvWarranty.Selected = True
        End If

    End Sub
End Class