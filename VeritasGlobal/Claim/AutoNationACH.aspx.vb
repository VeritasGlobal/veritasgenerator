﻿
Imports Telerik.Web.UI
Imports Telerik.Web.UI.Calendar

Public Class AutoNationACH
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        tbTodo.Width = pnlHeader.Width
        SqlDataSource1.ConnectionString = System.Configuration.ConfigurationManager.AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            CheckToDo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If hfUserID.Value = "1" Then
                btnLossCode.Visible = True
            Else
                btnLossCode.Visible = False
            End If
        End If
        If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
        Else
            pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
        End If
        FillPaymentTable()
        ReadOnlyButtons()
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                Response.Redirect("~/users/claimssearch.aspx?sid=" & hfID.Value)
            End If
        End If
    End Sub

    Private Sub CheckToDo()
        hlToDo.Visible = False
        hlToDo.NavigateUrl = "~\users\todoreader.aspx?sid=" & hfID.Value
        Dim SQL As String
        Dim clTD As New clsDBO
        SQL = "select * from usermessage "
        SQL = SQL + "where toid = " & hfUserID.Value & " "
        SQL = SQL + "and completedmessage = 0 "
        SQL = SQL + "and deletemessage = 0 "
        clTD.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clTD.RowCount > 0 Then
            hlToDo.Visible = True
        Else
            hlToDo.Visible = False
        End If

    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If

        End If
    End Sub

    Private Sub ShowError()
        hfError.Value = "Visible"
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).show(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "rwAgency", script, True)
    End Sub

    Private Sub btnErrorOK_Click(sender As Object, e As EventArgs) Handles btnErrorOK.Click
        hfError.Value = ""
        Dim script As String = "function f(){$find(""" + rwError.ClientID + """).hide(); Sys.Application.remove_load(f);}Sys.Application.add_load(f);"
        ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Key", script, True)
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx")
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub FillClaimTable()
        rgClaim.Rebind()
    End Sub

    Private Sub FillPaymentTable()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimautonationach "
        SQL = SQL + "where canaid > 0 "
        If Not rdpEndWeek.SelectedDate Is Nothing Then
            SQL = SQL + "and perioddate = '" & rdpEndWeek.SelectedDate & "' "
        End If
        SQL = SQL + "order by dateapprove "
        rgPayment.DataSource = clR.GetData(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        rgPayment.Rebind()
    End Sub

    Private Sub rgPayment_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgPayment.ItemCommand
        If e.CommandName = "ExportToCsv" Then
            rgPayment.ExportSettings.ExportOnlyData = False
            rgPayment.ExportSettings.IgnorePaging = True
            rgPayment.ExportSettings.OpenInNewWindow = True
            rgPayment.ExportSettings.UseItemStyles = True
            'rgDealer.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgPayment.MasterTableView.ExportToCSV()
        End If
        If e.CommandName = "DeleteRow" Then
            DeletePayment(rgPayment.Items(e.Item.ItemIndex).GetDataKeyValue("ClaimID"))
            FillPaymentTable()
            rgClaim.Rebind()
        End If
    End Sub

    Private Sub DeletePayment(xID As Long)
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "delete claimautonationach "
        SQL = SQL + "where claimid = " & xID
        clR.RunSQL(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
    End Sub

    Private Sub btnMoveToPayment_Click(sender As Object, e As EventArgs) Handles btnMoveToPayment.Click
        Dim gdi As Telerik.Web.UI.GridDataItem
        If rdpEndWeek.SelectedDate Is Nothing Then
            lblError2.Text = "No Period date is given."
            lblError2.Visible = True
            Exit Sub
        Else
            lblError2.Visible = False
        End If

        For Each gdi In rgClaim.SelectedItems
            AddPayment(gdi.GetDataKeyValue("ClaimID"))
        Next
        rgClaim.Rebind()
        FillPaymentTable()
    End Sub

    Private Sub AddPayment(xID As Long)
        Dim SQL As String
        Dim clP As New clsDBO
        Dim clR As New clsDBO
        SQL = "Select cl.claimid, cd.DateApprove, DealerName, d.DealerNo, c.FName + ' ' + c.LName as Customer, cl.RONumber, right(c.vin, 8) as VIN, cl.lossdate, cl.claimno, '' as ProductCode, sum(cd.TotalAmt) as PaidAmt "
        SQL = SQL + "from claim cl "
        SQL = SQL + "inner join claimdetail cd on cd.ClaimID = cl.ClaimID "
        SQL = SQL + "inner join ClaimPayee cp on cp.ClaimPayeeID = cd.ClaimPayeeID "
        SQL = SQL + "inner join ServiceCenter sc on cp.PayeeNo = sc.ServiceCenterNo "
        SQL = SQL + "inner join dealer d on d.DealerNo = sc.DealerNo "
        SQL = SQL + "inner join contract c on c.ContractID = cl.ContractID "
        SQL = SQL + "where d.dealerno like '2%' "
        SQL = SQL + "and cl.status = 'Open' and ClaimDetailStatus='Approved' "
        SQL = SQL + "and dateapprove > '9/15/2019' "
        SQL = SQL + "and cl.claimid = " & xID & " "
        SQL = SQL + "group by cl.claimid, cd.DateApprove, DealerName, d.DealerNo, cl.RONumber, right(c.vin, 8), cl.lossdate, cl.claimno,c.FName + ' ' + c.LName "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            SQL = "select * from claimautonationach "
            SQL = SQL + "where claimid = " & xID & " "
            clP.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
            If clP.RowCount = 0 Then
                clP.NewRow()
            Else
                clP.GetRow()
            End If
            clP.Fields("perioddate") = rdpEndWeek.SelectedDate
            clP.Fields("claimid") = xID
            clP.Fields("dateapprove") = clR.Fields("dateapprove")
            clP.Fields("dealername") = clR.Fields("dealername")
            clP.Fields("dealerno") = clR.Fields("dealerno")
            clP.Fields("ronumber") = clR.Fields("ronumber")
            clP.Fields("customer") = clR.Fields("customer")
            clP.Fields("vin") = clR.Fields("vin")
            clP.Fields("lossdate") = clR.Fields("lossdate")
            clP.Fields("claimno") = clR.Fields("claimno")
            clP.Fields("Paidamt") = clR.Fields("paidamt")
            If clP.RowCount = 0 Then
                clP.AddRow()
            End If
            clP.SaveDB()
        End If

    End Sub

    Private Sub rdpEndWeek_SelectedDateChanged(sender As Object, e As SelectedDateChangedEventArgs) Handles rdpEndWeek.SelectedDateChanged
        FillPaymentTable()
    End Sub

    Private Sub rgClaim_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgClaim.ItemCommand
        If e.CommandName = "ExportToCsv" Then
            rgClaim.ExportSettings.ExportOnlyData = False
            rgClaim.ExportSettings.IgnorePaging = True
            rgClaim.ExportSettings.OpenInNewWindow = True
            rgClaim.ExportSettings.UseItemStyles = True
            'rgDealer.ExportSettings.Excel.Format = DirectCast([Enum].Parse(GetType(GridExcelExportFormat), "Xlsx"), GridExcelExportFormat)
            rgClaim.MasterTableView.ExportToCSV()
        End If
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketMessage_Click(sender As Object, e As EventArgs) Handles btnTicketMessage.Click
        Response.Redirect("~/claim/ClaimTicketMessage.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnTicketResponse_Click(sender As Object, e As EventArgs) Handles btnTicketResponse.Click
        Response.Redirect("~/claim/ClaimTicketResponse.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSettings_Click(sender As Object, e As EventArgs) Handles btnSettings.Click
        Response.Redirect("https://veritasgenerator.com/settings/settings.aspx?sid=" & hfID.Value)
    End Sub
End Class