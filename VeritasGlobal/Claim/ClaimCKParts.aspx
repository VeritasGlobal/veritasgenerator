﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimCKParts.aspx.vb" Inherits="VeritasGlobal.ClaimCKParts" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell>
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Quote ID:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtQuoteID" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Order ID:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtOrderID" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell Font-Bold="true">
                                        Sent Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:TextBox ID="txtSentDate" runat="server"></asp:TextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnRebuild" runat="server" Text="Rebuild Quote" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell>
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgPayment">
                                <telerik:RadGrid ID="rgPayment" runat="server" AutoGenerateColumns="false"
                                    AllowSorting="true" AllowPaging="false" ShowFooter="true"  MasterTableView-CommandItemDisplay="TopAndBottom"
                                        Font-Names="Calibri" Font-Size="Small" DataSourceID="dsPart">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" ShowFooter="true" ShowHeader="true" Width="1500" DataKeyNames="ClaimDetailID" >
                                        <CommandItemTemplate>
                                            <telerik:RadButton ID="updateBtn" runat="server" Text="Update Edited" CommandName="UpdateEdited"></telerik:RadButton>
                                            <telerik:RadButton ID="cancelBtn" runat="server" Text="Cancel All" CommandName="CancelAll"></telerik:RadButton>
                                        </CommandItemTemplate>
                                        <CommandItemSettings ShowExportToExcelButton="false" ShowExportToCsvButton="false" ShowAddNewRecordButton="false" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimDetailID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="JobNo" AutoPostBackOnFilter="true" UniqueName="DateApproved" HeaderText="Job No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PartName" UniqueName="PartName" HeaderText="Part Name"></telerik:GridBoundColumn>
                                            <telerik:GridTemplateColumn DataField="Qty" DataType="System.Decimal" ReadOnly="True" />
                                            <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="ReqAmt" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="YourCost" UniqueName="YourCost" HeaderText="YourCost" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TotalCost" UniqueName="TotalCost" HeaderText="TotalCost" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridButtonColumn CommandName="DeleteRow" CommandArgument="ClaimID" ButtonType="ImageButton" ImageUrl="~/images/delete.png"></telerik:GridButtonColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsPart" runat="server" SelectCommand="select cd.claimdetailid, cd.jobno, cd.partno, cd.partname, 
                                    cd.qty, cd.reqamt, cd.yourcost, cd.totalcost from ckpart cd inner join ckquote cq on cd.ckid = cq.ckid 
                                    where claimid = @claimid">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </telerik:RadAjaxPanel>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow>
                        <asp:TableCell HorizontalAlign="Right">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Total Part Cost:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtTotalPartCost" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Shipping Cost:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtShippingCost" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        Total Cost:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtTotalCost" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                    </asp:TableRow>
                    <asp:TableRow HorizontalAlign="Right">
                        <asp:TableCell>
                            <asp:Button ID="btnPurchase" runat="server" Text="Purchase" BackColor="#1eabe2" />
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                <asp:HiddenField ID="hfClaimID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
