﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimAlert1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            FillPage()
        End If

    End Sub

    Private Sub FillPage()
        Dim SQL As String
        Dim clR As New clsDBO
        Dim clA As New clsDBO
        SQL = "select * from dealernote dn "
        SQL = SQL + "inner join contract c on c.dealerid = dn.dealerid "
        SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
        SQL = SQL + "where claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (dn.claimalways <> 0 "
        SQL = SQL + "or claimnewentry <> 0) "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                txtDealerNote.Text = txtDealerNote.Text & clR.Fields("note") & vbCrLf & vbCrLf
            Next
        End If

        SQL = "Select * from agentsnote an "
        SQL = SQL + "inner join Dealer d On d.agentsid = an.agentid "
        SQL = SQL + "inner join contract c On c.dealerid = d.dealerid "
        SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (an.claimalways <> 0 "
        SQL = SQL + "or an.claimnewentry <> 0) "
        clA.OpenDB(SQL, AppSettings("connstring"))
        If clA.RowCount > 0 Then
            For cnt = 0 To clA.RowCount - 1
                clA.GetRowNo(cnt)
                'pvDealer.Selected = True
                txtDealerNote.Text = txtDealerNote.Text & clA.Fields("note") & vbCrLf & vbCrLf
            Next
        End If

        SQL = "Select * from subagentnote an "
        SQL = SQL + "inner join Dealer d On d.subagentid = an.subagentid "
        SQL = SQL + "inner join contract c On c.dealerid = d.dealerid "
        SQL = SQL + "inner join claim cl on c.contractid = cl.contractid "
        SQL = SQL + "where cl.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and (an.claimalways <> 0 "
        SQL = SQL + "or an.claimnewentry <> 0) "
        clA.OpenDB(SQL, AppSettings("connstring"))
        If clA.RowCount > 0 Then
            For cnt = 0 To clA.RowCount - 1
                clA.GetRowNo(cnt)
                'pvDealer.Selected = True
                txtDealerNote.Text = txtDealerNote.Text & clA.Fields("note") & vbCrLf & vbCrLf
            Next
        End If
    End Sub

End Class