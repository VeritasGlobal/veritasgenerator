﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ClaimGapLoanInfo
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        dsStates.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            FillBank()
        End If
    End Sub

    Private Sub FillBank()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgaploaninfo "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtFName.Text = clR.Fields("bankname")
            txtAddr1.Text = clR.Fields("addr1")
            txtAddr2.Text = clR.Fields("addr2")
            txtCity.Text = clR.Fields("city")
            cboState.SelectedValue = clR.Fields("state")
            txtZip.Text = clR.Fields("zip")
            txtPhone.Text = clR.Fields("phone")
            txtEMail.Text = clR.Fields("email")
            txtAccountNo.Text = clR.Fields("accountno")
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgaploaninfo "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            clR.NewRow()
            clR.Fields("claimgapid") = hfClaimID.Value
            clR.Fields("creby") = hfUserID.Value
            clR.Fields("credate") = Today
        Else
            clR.GetRow()
            clR.Fields("moddate") = Today
            clR.Fields("modby") = hfUserID.Value
        End If
        clR.Fields("bankname") = txtFName.Text
        clR.Fields("addr1") = txtAddr1.Text
        clR.Fields("addr2") = txtAddr2.Text
        clR.Fields("city") = txtCity.Text
        clR.Fields("state") = cboState.SelectedValue
        clR.Fields("zip") = txtZip.Text
        clR.Fields("phone") = txtPhone.Text
        clR.Fields("email") = txtEMail.Text
        clR.Fields("accountno") = txtAccountNo.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub
End Class