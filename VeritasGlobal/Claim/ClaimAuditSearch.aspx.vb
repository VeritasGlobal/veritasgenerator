﻿Public Class ClaimAuditSearch
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            GetServerInfo()
            If hfUserID.Value = 0 Then
                Response.Redirect("~/default.aspx")
            End If
            If System.Configuration.ConfigurationManager.AppSettings("connstring").Contains("test") Then
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1a4688")
            Else
                pnlHeader.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
                Image1.BackColor = Drawing.ColorTranslator.FromHtml("#1eabe2")
            End If
            If hfUserID.Value = "1" Then
                btnLossCode.Visible = True
            Else
                btnLossCode.Visible = False
            End If
            ReadOnlyButtons()
            btnRFClaimSubmit.Enabled = False
        End If

    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("readonly") = "True" Then
                btnServiceCenters.Enabled = False
                btnClaimsOpen.Enabled = False
                btnAutonationACH.Enabled = False
            End If
            If CBool(clR.Fields("AllowClaimAudit")) Then
                btnClaimAudit.Enabled = True
            Else
                btnClaimAudit.Enabled = False
            End If
            If CBool(clR.Fields("teamlead")) Then
                btnClaimTeamOpen.Enabled = True
            Else
                btnClaimTeamOpen.Enabled = False
            End If
            hfVeroOnly.Value = CBool(clR.Fields("veroonly"))
            hfAgentID.Value = clR.Fields("agentid")
            hfSubAgentID.Value = clR.Fields("subagentid")
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Date.Today
        sEndDate = DateAdd("d", 1, Date.Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        hfUserID.Value = 0
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
            LockButtons()
            Unlockbuttons()
        End If
    End Sub

    Private Sub LockButtons()
        btnAccounting.Enabled = False
        btnAgents.Enabled = False
        btnClaim.Enabled = False
        btnDealer.Enabled = False
        btnContract.Enabled = False
        btnSettings.Enabled = False
        btnUsers.Enabled = False
        btnUsers.Enabled = False
        btnContract.Enabled = False
        btnReports.Enabled = False
        btnUnlockClaim.Enabled = False
    End Sub

    Private Sub Unlockbuttons()
        Dim SQL As String
        Dim clSI As New clsDBO
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfInsCarrierID.Value = clSI.Fields("inscarrierid")
            btnUsers.Enabled = True
            If clSI.Fields("accounting") = True Then
                btnAccounting.Enabled = True
            End If
            If clSI.Fields("Settings") = True Then
                btnSettings.Enabled = True
            End If
            If clSI.Fields("Agents") = True Then
                btnAgents.Enabled = True
            End If
            If clSI.Fields("Dealer") = True Then
                btnDealer.Enabled = True
            End If
            If clSI.Fields("claim") = True Then
                btnClaim.Enabled = True
            End If
            If clSI.Fields("contract") = True Then
                btnContract.Enabled = True
            End If
            If clSI.Fields("salesreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("accountreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("claimsreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("customreports") = True Then
                btnReports.Enabled = True
            End If
            If clSI.Fields("unlockclaim") = True Then
                btnUnlockClaim.Enabled = True
            End If
            If clSI.Fields("claimpayment") = False Then
                btnInspectionWex.Enabled = False
                btnCarfaxPayment.Enabled = False
            End If
        End If
    End Sub

    Private Sub btnUsers_Click(sender As Object, e As EventArgs) Handles btnUsers.Click
        Response.Redirect("~/users/users.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLogOut_Click(sender As Object, e As EventArgs) Handles btnLogOut.Click
        Response.Redirect("~/default.aspx")
    End Sub

    Private Sub btnHome_Click(sender As Object, e As EventArgs) Handles btnHome.Click
        Response.Redirect("~/default.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAgents_Click(sender As Object, e As EventArgs) Handles btnAgents.Click
        Response.Redirect("~/agents/AgentsSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnDealer_Click(sender As Object, e As EventArgs) Handles btnDealer.Click
        Response.Redirect("~/dealer/dealersearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnReports_Click(sender As Object, e As EventArgs) Handles btnReports.Click
        Response.Redirect("~/reports/reports.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnContract_Click(sender As Object, e As EventArgs) Handles btnContract.Click
        Response.Redirect("~/contract/ContractSearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAccounting_Click(sender As Object, e As EventArgs) Handles btnAccounting.Click
        Response.Redirect("~/accounting/accounting.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaim_Click(sender As Object, e As EventArgs) Handles btnClaim.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnServiceCenters_Click(sender As Object, e As EventArgs) Handles btnServiceCenters.Click
        Response.Redirect("~/claim/servicecenters.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnAutonationACH_Click(sender As Object, e As EventArgs) Handles btnAutonationACH.Click
        Response.Redirect("~/claim/autonationach.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimSearch_Click(sender As Object, e As EventArgs) Handles btnClaimSearch.Click
        Response.Redirect("~/claim/claimsearch.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnSearch_Click(sender As Object, e As EventArgs) Handles btnSearch.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select ca.claimauditid, c.ClaimNo, uia.FName as adjfname, uia.LName as adjlname, uir.FName as revfirst, uir.LName as revlast, ca.CreDate as RevDate from claimaudit ca
        inner join userinfo uia on ca.AdjusterID = uia.UserID
        inner join userinfo uir on ca.ReviewerID = uir.UserID
        inner join claim c on c.claimid = ca.ClaimID "
        SQL = SQL + "where claimauditid > 0 "
        If txtClaimNo.Text.Length > 0 Then
            SQL = SQL + "and claimno like '%" & txtClaimNo.Text.Trim & "%' "
        End If
        If txtAgentFName.Text.Length > 0 Then
            SQL = SQL + "and uia.fname like '%" & txtAgentFName.Text.Trim & "%' "
        End If
        If txtAgentLName.Text.Length > 0 Then
            SQL = SQL + "and uia.lname like '%" & txtAgentLName.Text.Trim & "%' "
        End If

    End Sub

    Private Function CheckANOnly() As Boolean
        Dim SQL As String
        Dim clR As New clsDBO
        CheckANOnly = False
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and autonationonly <> 0 "
        clR.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckANOnly = True
        End If
    End Function


    Private Sub btnUnlockClaim_Click(sender As Object, e As EventArgs) Handles btnUnlockClaim.Click
        Response.Redirect("~/claim/unlockclaim.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnInspectionWex_Click(sender As Object, e As EventArgs) Handles btnInspectionWex.Click
        Response.Redirect("~/claim/ClaimWexInspection.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnLossCode_Click(sender As Object, e As EventArgs) Handles btnLossCode.Click
        Response.Redirect("~/claim/ClaimLossCode.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimsOpen_Click(sender As Object, e As EventArgs) Handles btnClaimsOpen.Click
        Response.Redirect("~/claim/claimsopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimTeamOpen_Click(sender As Object, e As EventArgs) Handles btnClaimTeamOpen.Click
        Response.Redirect("~/claim/claimteamopen.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnRFClaimSubmit_Click(sender As Object, e As EventArgs) Handles btnRFClaimSubmit.Click
        Response.Redirect("~/claim/RFClaimSubmit.aspx?sid=" & hfID.Value)
    End Sub

    Private Sub btnClaimAudit_Click(sender As Object, e As EventArgs) Handles btnClaimAudit.Click
        Response.Redirect("~/claim/ClaimAuditSearch.aspx?sid=" & hfID.Value)
    End Sub
End Class