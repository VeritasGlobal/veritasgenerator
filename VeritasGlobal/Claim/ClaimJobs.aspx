﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimJobs.aspx.vb" Inherits="VeritasGlobal.ClaimJobs1" %>

<%@ Register Assembly="Telerik.Web.UI" Namespace="Telerik.Web.UI" TagPrefix="telerik" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel ID="UpdatePanel1" runat="server">
            <ContentTemplate>
                <asp:Panel runat="server" ID="pnlList" DefaultButton="btnHiddenList">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell ID="tcManagerAuth">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAddDetail" runat="server" Text="Add Detail" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboJobNo" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnAuthIt" runat="server" Text="Authorize" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnApproveIt" runat="server" Text="Approve" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnDenyIt" runat="server" Text="Deny" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell ID="tcManageSlush">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboSlushJobNo" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Slush:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboSlushRateType" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnUpdateSlush" runat="server" Text="Update Slush" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblAuthorizedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Label ID="lblApprovedError" runat="server" ForeColor="Red" Font-Bold="true" Font-Size="X-Large"></asp:Label>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                    <telerik:RadGrid ID="rgJobs" runat="server" AutoGenerateColumns="false" DataSourceID="dsJobs" 
                                        AllowSorting="true" Width="1000" ShowFooter="true">
                                        <MasterTableView AutoGenerateColumns="false" DataKeyNames="ClaimDetailID" ShowFooter="true">
                                            <GroupByExpressions>
                                                <telerik:GridGroupByExpression>
                                                    <SelectFields>
                                                        <telerik:GridGroupByField FieldAlias="JobNo" FieldName="JobNo" HeaderText="Job No" HeaderValueSeparator=": " />
                                                        <telerik:GridGroupByField FieldAlias="LossCodDesc" FieldName="LossCodeDesc" HeaderValueSeparator=": " />
                                                    </SelectFields>
                                                    <GroupByFields>
                                                        <telerik:GridGroupByField FieldName="JobNo" SortOrder="Ascending" />
                                                        <telerik:GridGroupByField FieldName="LossCodeDesc" SortOrder="Ascending" />
                                                    </GroupByFields>
                                                </telerik:GridGroupByExpression>
                                            </GroupByExpressions>
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="ClaimDetailID"  ReadOnly="true" Visible="false" UniqueName="ClaimPartID"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDetailType" UniqueName="ClaimDetailType" HeaderText="Detail Type"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDetailStatus" UniqueName="ClaimDetailStatus" HeaderText="Status"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="JobNo" UniqueName="JobNo" HeaderText="Job No"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PartNo" UniqueName="PartNo" HeaderText="Part No"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ClaimDesc" UniqueName="ClaimDesc" HeaderText="Claim Desc"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="LossCode" UniqueName="LossCode" HeaderText="Loss Code"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PayeeName" UniqueName="PayeeName" HeaderText="Payee Name"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="ReqAmt" UniqueName="ReqAmt" HeaderText="Req Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="AuthAmt" UniqueName="AuthAmt" HeaderText="Auth Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TaxAmt" UniqueName="TaxAmt" HeaderText="Tax Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TotalAmt" UniqueName="TotalAmt" HeaderText="Total Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="PaidAmt" UniqueName="PaidAmt" HeaderText="PaidAmt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                        <ClientSettings EnablePostBackOnRowClick="true">
                                            <Selecting AllowRowSelect="true" />
                                        </ClientSettings>
                                    </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsJobs" ProviderName="System.Data.SqlClient" 
                                    SelectCommand="select claimdetailid, claimdetailtype,cd.partno, cd.claimdesc, claimdetailstatus, jobno, cd.losscode, lc.losscodedesc, payeename, reqamt, authamt, taxamt, paidamt, totalamt
                                    from claimdetail cd left join claimpayee cp on cp.claimpayeeid = cd.claimpayeeid 
                                    left join claimlosscode lc on lc.losscode = cd.losscode where claimid =  @ClaimID order by jobno" 
                                    runat="server">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenList" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Totals:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPayeeTotal" Width="250" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Jobs:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboJobsTotal" Width="250" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                    <telerik:RadGrid ID="rgClaimTotal" runat="server" AutoGenerateColumns="false" 
                                        AllowSorting="true" Width="1000" ShowFooter="true">
                                        <MasterTableView AutoGenerateColumns="false" ShowFooter="true">
                                            <Columns>
                                                <telerik:GridBoundColumn DataField="ClaimProcess" UniqueName="ClaimProcess" HeaderText="Process"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Part" UniqueName="Part" HeaderText="Part" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Labor" UniqueName="Labor" HeaderText="Labor" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Other" UniqueName="Other" HeaderText="Other" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="SubTotal" UniqueName="SubTotal" HeaderText="Sub Total" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="TaxAmt" UniqueName="TaxAmt" HeaderText="Tax Amt" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Deduct" UniqueName="Deduct" HeaderText="Deduct" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                                <telerik:GridBoundColumn DataField="Total" UniqueName="Total" HeaderText="Total" DataFormatString="{0:C}"></telerik:GridBoundColumn>
                                            </Columns>
                                        </MasterTableView>
                                    </telerik:RadGrid>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlDetail" DefaultButton="btnHiddenDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell runat="server">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Detail Type:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboClaimDetailType" DataSourceID="dsCDT" AutoPostBack="true" DataTextField="ClaimDetailType" DataValueField="ClaimDetailType" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsCDT"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailType from claimdetail group by ClaimDetailType order by ClaimDetailType" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Job No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtJobNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Detail Status:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboClaimDetailStatus" DataValueField="ClaimDetailStatus" AutoPostBack="true" DataSourceID="dsClaimDetailStatus" DataTextField="ClaimDetailStatusDesc" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsClaimDetailStatus" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimDetailStatus, ClaimDetailStatusDesc from ClaimDetailStatus" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Reason Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboReason" DataValueField="ClaimReasonID" DataSourceID="dsClaimReason" DataTextField="ReasonDesc" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsClaimReason" ProviderName="System.Data.SqlClient" SelectCommand="select ClaimReasonID, ReasonDesc from ClaimReason order by reasondesc" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Part No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPartNo" Enabled="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimDesc" Enabled="true" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCode" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekLossCode" runat="server" Text="Seek Loss Code" />
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Loss Code Desc:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtLossCodeDesc" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayeeName" runat="server"></asp:TextBox>
                                            <asp:Button ID="btnSeekPayee" runat="server" Text="Seek Payee" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Req Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqQty" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Req Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqCost" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Req Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtReqAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Qty:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthQty" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Auth Cost:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthCost" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Tax Rate:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTaxRate" AutoPostBack="true" Type="Percent" NumberFormat-DecimalDigits="4" runat="server" ></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Tax Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTaxAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Total Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtTotalAmt" ReadOnly="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Authorized Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtAuthAmt" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Paid Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadNumericTextBox ID="txtPaidAmt" ReadOnly="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Authorized Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpAuthorizedDate" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Approved Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDateApprove" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Paid Date:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDatePicker ID="rdpDatePaid" runat="server"></telerik:RadDatePicker>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Account:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboRateType0" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenDetail" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnMakePayment" BackColor="#1eabe2" runat="server" Text="Make Payment" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCancelClaimDetail" BackColor="#1eabe2" runat="server" Text="Cancel" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSaveClaimDetail" BackColor="#1eabe2" runat="server" Text="Save" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlLossCode" DefaultButton="btnHiddenLC">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgLossCode" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" 
                                    AllowSorting="true"  Width="1000" ShowFooter="true" DataSourceID="dsLossCode">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="LossCode" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="LossCodeID"  ReadOnly="true" Visible="false" UniqueName="LossCodeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCode" FilterCheckListWebServiceMethod="LoadLossCode" UniqueName="LossCode" HeaderText="Loss Code" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LossCodeDesc" FilterCheckListWebServiceMethod="LoadLossCodeDesc" UniqueName="LossCodeDesc" HeaderText="Loss Code Desc" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="ACP" UniqueName="ACP" HeaderText="ACP"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsLossCode" ProviderName="System.Data.SqlClient" runat="server" 
                                    SelectCommand="(select clc.*, 0 as ACP from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid 
									and not clc.losscode in ('ZDIAGTME','ZGOODWLL','ZGWCLAIM','ZLABRTME','ZLBRDIFF', 'ZPARTCST','ZANGDWLL','ZMCEDIFF')
									union 
									select clc.*, 1 as ACP from ClaimLossCode ClC left join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    where not LossCodePrefix in(
									select LossCodePrefix from ClaimLossCode ClC inner join contractcoverage cc on cc.CoverageCode = clc.LossCodePrefix
                                    inner join contract c on c.ProgramID = cc.ProgramID and c.PlanTypeID = cc.PlanTypeID inner join claim cl on cl.ContractID = c.ContractID where cl.claimid = @claimid 
									and not clc.losscode in ('ZDIAGTME','ZGOODWLL','ZGWCLAIM','ZLABRTME','ZLBRDIFF', 'ZPARTCST','ZANGDWLL','ZMCEDIFF'))
									) order by losscode">
                                    <SelectParameters>
                                        <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                    </SelectParameters>
                                </asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenLC" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel runat="server" ID="pnlSeekPayee" DefaultButton="btnHiddenCP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Button ID="btnAddClaimPayee" runat="server" Text="Add Claim Payee" BackColor="#1eabe2" />
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <telerik:RadGrid ID="rgClaimPayee" runat="server" AutoGenerateColumns="false" AllowFilteringByColumn="true" DataSourceID="dsClaimPayee"
                                    AllowSorting="true" Width="1000" ShowFooter="true">
                                    <GroupingSettings CaseSensitive="false" />
                                    <MasterTableView AutoGenerateColumns="false" AllowFilteringByColumn="true" DataKeyNames="ClaimPayeeID" ShowFooter="true">
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimPayeeID"  ReadOnly="true" Visible="false" UniqueName="ClaimPayeeID"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeNo" FilterCheckListWebServiceMethod="LoadPayeeNo" UniqueName="PayeeNo" HeaderText="Payee No" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PayeeName" FilterCheckListWebServiceMethod="LoadPayeeName" UniqueName="PayeeName" HeaderText="Payee Name" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="City" FilterCheckListWebServiceMethod="LoadCity" UniqueName="DealerCity" HeaderText="City" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="State" FilterCheckListWebServiceMethod="LoadState" UniqueName="State" HeaderText="State" AutoPostBackOnFilter="true" CurrentFilterFunction="Contains" ShowFilterIcon="true"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                    <ClientSettings EnablePostBackOnRowClick="true">
                                        <Selecting AllowRowSelect="true" />
                                    </ClientSettings>
                                </telerik:RadGrid>
                                <asp:SqlDataSource ID="dsClaimPayee"
                                ProviderName="System.Data.SqlClient" SelectCommand="select claimpayeeid, payeeno, payeename, city, state from claimpayee" runat="server"></asp:SqlDataSource>
                            </asp:TableCell>
                            <asp:TableCell Visible="false">
                                <asp:Button ID="btnHiddenCP" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>
                <asp:Panel ID="pnlAddClaimPayee" runat="server" DefaultButton="btnHiddenACP">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Service Center Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtServiceCenterName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 1:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr1" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            Address 2:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtAddr2" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            City:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            State:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadComboBox ID="cboState" DataSourceID="dsStates" DataTextField="Abbr" DataValueField="Abbr" runat="server"></telerik:RadComboBox>
                                            <asp:SqlDataSource ID="dsStates"
                                            ProviderName="System.Data.SqlClient" SelectCommand="select abbr, stateid from states order by abbr" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Zip Code:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtZip" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Phone:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadMaskedTextBox ID="txtPhone" Mask="(###) ###-####" runat="server"></telerik:RadMaskedTextBox>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell HorizontalAlign="Right">
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Visible="false">
                                            <asp:Button ID="btnHiddenACP" runat="server" Text="Cancel" BackColor="#1eabe2" Visible="false" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCCancel" runat="server" Text="Cancel" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnSCSave" runat="server" Text="Save" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:Panel runat="server" ID="pnlToBePaidDetail">
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Claim Payee Name:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtClaimPayeeName" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Amt:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtPayAmt" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Payment Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:DropDownList ID="cboPaymentMethod" AutoPostBack="true" runat="server"></asp:DropDownList>
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell>
                                <asp:Table runat="server">
                                    <asp:TableRow ID="trACH">
                                        <asp:TableCell Font-Bold="true">
                                            Confirmation No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtConfirmNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessACH" runat="server" Text="Process ACH" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCheck">
                                        <asp:TableCell Font-Bold="true">
                                            Check No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCheckNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCheck" runat="server" Text="Process Check" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trCC">
                                        <asp:TableCell Font-Bold="true">
                                            Credit Card No:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtCCNo" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessCC" runat="server" Text="Process Credit Card" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow ID="trWex">
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Method:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <telerik:RadDropDownList ID="cboWexMethod" DataSourceID="dsWexMethod" DataTextField="wexmethod" DataValueField="wexmethod" runat="server"></telerik:RadDropDownList>
                                            <asp:SqlDataSource ID="dsWexMethod" ProviderName="System.Data.SqlClient"
                                                 SelectCommand="select wexmethod from wexmethod" runat="server"></asp:SqlDataSource>
                                        </asp:TableCell>
                                        <asp:TableCell Font-Bold="true">
                                            Wex Delivery Addess:
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:TextBox ID="txtWexAddress" runat="server"></asp:TextBox>
                                        </asp:TableCell>
                                        <asp:TableCell>
                                            <asp:Button ID="btnProcessWex" runat="server" Text="Process Wex" BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                    <asp:TableRow>
                                        <asp:TableCell>
                                            <asp:Button ID="btnCloseToBePaidDetail" runat="server" Text="Close"  BackColor="#1eabe2" />
                                        </asp:TableCell>
                                    </asp:TableRow>
                                </asp:Table>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </asp:Panel>

                <asp:HiddenField ID="hfSlushNote" runat="server" />
                <telerik:RadWindow ID="rwSlushNote"  runat="server" Width="500" Title="Note" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:Label runat="server" ID="Label1" Text="You need to enter in Note."></asp:Label>
                                </asp:TableCell>
                            </asp:TableRow>
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtSlushNote" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnCloseSlushNote" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="Cancel" />
                                </asp:TableCell>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnSaveSlushNote" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="Save" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>

                <asp:HiddenField ID="hfFieldCheck" runat="server" />
                <telerik:RadWindow ID="rwFieldCheck"  runat="server" Width="500" Title="Note" Height="300" Behaviors="Close" EnableViewState="false" ReloadOnShow="true">
                    <ContentTemplate>
                        <asp:Table runat="server" Height="60">
                            <asp:TableRow>
                                <asp:TableCell>
                                    <asp:TextBox ID="txtFieldChk" TextMode="MultiLine" Width="450" Height="175" runat="server"></asp:TextBox>
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                        <asp:Table runat="server" Width="400"> 
                            <asp:TableRow>
                                <asp:TableCell HorizontalAlign="Right">
                                    <asp:Button ID="btnClose" runat="server" ForeColor="White" BackColor="#1a4688" BorderColor="#1a4688" Width="75" Text="Close" />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ContentTemplate>
                </telerik:RadWindow>


                <asp:HiddenField ID="hfID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfClaimDetailID" runat="server" />
                <asp:HiddenField ID="hfClaimPayeeID" runat="server" />
                <asp:HiddenField ID="hfJobNo" runat="server" />
                <asp:HiddenField ID="hfServiceCenterNo" runat="server" />
                <asp:HiddenField ID="hfClaimPaymentID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCCno" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfWexCode" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCompanyNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfClaimNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfRONo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractNo" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfCustomerName" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfVIN" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfPayeeContact" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfContractID" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfTicket" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfAllowAZDenied" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfOldTotalAmt" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="hfStatus" runat="server"></asp:HiddenField>
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
