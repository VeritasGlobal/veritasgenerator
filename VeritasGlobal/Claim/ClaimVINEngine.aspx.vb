﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimVINEngine1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("Contractid")
        If Not IsPostBack Then
            GetVIN()
            If hfVIN.Value.Length > 0 Then
                FillPage()
            End If
        End If
    End Sub

    Private Sub GetVIN()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select vin from contract c "
        SQL = SQL + "where Contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfVIN.Value = clC.Fields("vin")
        End If
    End Sub

    Private Sub FillPage()
        Dim SQL As String
        Dim clV As New clsDBO
        SQL = "select * from vin.dbo.vin v "
        SQL = SQL + "inner join vin.dbo.engine bd on bd.vinid = v.vinid "
        SQL = SQL + "where vin = '" & hfVIN.Value.Substring(0, 11) & "' "
        clV.OpenDB(SQL, AppSettings("connstring"))
        If clV.RowCount > 0 Then
            clV.GetRow()
            txtName.Text = clV.Fields("name")
            txtAvailability.Text = clV.Fields("availability")
            txtAspiration.Text = clV.Fields("aspiration")
            txtBlockType.Text = clV.Fields("blocktype")
            txtBore.Text = clV.Fields("bore")
            txtCamType.Text = clV.Fields("camtype")
            txtCompression.Text = clV.Fields("compression")
            txtCyclinders.Text = clV.Fields("cyclinders")
            txtDisplacement.Text = clV.Fields("displacement")
            txtElectricMotorConfig.Text = clV.Fields("electricmotorconfig")
            txtElectricMaxHP.Text = clV.Fields("electricmaxhp")
            txtElectricMaxKW.Text = clV.Fields("electricmaxhp")
            txtElectricMaxTorque.Text = clV.Fields("electricmaxtorque")
            txtEngineType.Text = clV.Fields("enginetype")
            txtFuelInduction.Text = clV.Fields("fuelinduction")
            txtFuelQuality.Text = clV.Fields("fuelquality")
            txtFuelType.Text = clV.Fields("FuelType")
            chkFleet.Checked = CBool(clV.Fields("fleet"))
            txtGeneratorDesc.Text = clV.Fields("generatordesc")
            txtGeneratorMaxHP.Text = clV.Fields("generatormaxhp")
            txtMaxHP.Text = clV.Fields("maxhp")
            txtMaxHPatRPM.Text = clV.Fields("maxhpatrpm")
            txtMaxPayload.Text = clV.Fields("maxpayload")
            txtMaxTorque.Text = clV.Fields("maxtorque")
            txtMaxTorqueatRPM.Text = clV.Fields("maxtorqueatrpm")
            txtOilCapacity.Text = clV.Fields("oilcapacity")
            txtRedLine.Text = clV.Fields("redline")
            txtStroke.Text = clV.Fields("totalmaxhp")
            txtValves.Text = clV.Fields("valves")
            txtValveTiming.Text = clV.Fields("valvetiming")
            txtTotalMaxHP.Text = clV.Fields("totalmaxhp")
            txtTotalMaxHPatRPM.Text = clV.Fields("totalmaxhpatrpm")
            txtTotalMaxTorque.Text = clV.Fields("totalmaxtorque")
            txtTotalMaxTorqueatRPM.Text = clV.Fields("TotalMaxTorqueatRPM")
            txtOrderCode.Text = clV.Fields("ordercode")
        End If

    End Sub


End Class