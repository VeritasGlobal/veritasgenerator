﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimCustomer1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("contractid")
        dsStates.ConnectionString = AppSettings("connstring")
        If Not IsPostBack Then
            GetServerInfo()
            FillCustomer()
            btnUpdateCustomer.Visible = True
            ReadOnlyButtons()
        End If
    End Sub

    Private Sub ReadOnlyButtons()
        Dim SQL As String
        Dim clR As New clsDBO
        btnUpdateCustomer.Enabled = True
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and readonly <> 0 "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            btnUpdateCustomer.Enabled = False
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillCustomer()
        GetContract()
    End Sub

    Private Sub GetContract()
        Dim clC As New clsDBO
        Dim SQL As String
        If hfContractID.Value.Length = 0 Then
            Exit Sub
        End If
        SQL = "select fname, lname, addr1, addr2, city, state, zip, phone "
        SQL = SQL + "from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            txtFName.Text = clC.Fields("fname")
            txtLName.Text = clC.Fields("lname")
            txtAddr1.Text = clC.Fields("addr1")
            txtAddr2.Text = clC.Fields("addr2")
            txtCity.Text = clC.Fields("city")
            cboState.SelectedValue = clC.Fields("state")
            txtZip.Text = clC.Fields("zip")
            txtPhone.Text = clC.Fields("phone")
        End If
    End Sub

    Private Sub btnUpdateCustomer_Click(sender As Object, e As EventArgs) Handles btnUpdateCustomer.Click
        Dim SQL As String
        Dim clC As New clsDBO
        Dim clMAC As New VeritasGlobalTools.clsMoxyAddressChange
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            clC.Fields("fname") = txtFName.Text
            clC.Fields("lname") = txtLName.Text
            clC.Fields("addr1") = txtAddr1.Text
            clC.Fields("addr2") = txtAddr2.Text
            clC.Fields("city") = txtCity.Text
            clC.Fields("state") = cboState.SelectedValue
            clC.Fields("zip") = txtZip.Text
            clC.Fields("phone") = txtPhone.Text
            clC.SaveDB()
            clMAC.UpdateMoxy(hfContractID.Value)
        End If
    End Sub


End Class