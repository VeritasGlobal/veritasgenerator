﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimGAPFinance.aspx.vb" Inherits="VeritasGlobal.ClaimGAPFinance" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
            <ContentTemplate>
                <asp:Table runat="server">
                    <asp:TableRow>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Vehicle Purchase Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpVehiclePurchaseDate" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        First Payment Date:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadDatePicker ID="rdpLoanStartDate" AutoPostBack="true" runat="server"></telerik:RadDatePicker>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Trade In:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtTradeIn" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Total Cash Price:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtTotalCashPrice" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Down Payment:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtDownPayment" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Amount Finance:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtAmtFinance" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Annual Percentage:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtAnnualPer" Type="Percent" MinValue="0" MaxValue="100" NumberFormat-DecimalDigits="3" AutoPostBack="true" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkMonthly" Text="Monthly" AutoPostBack="true" runat="server" />
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:CheckBox ID="chkBiWeekly" Text="Bi-weekly" AutoPostBack="true" runat="server" />
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        No Payments:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtNoPayments" AutoPostBack="true" NumberFormat-DecimalDigits="0" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <asp:Button ID="btnCalLoan" runat="server" Text="Calc Loan" BackColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <asp:Table runat="server">
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Finance Charge:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtFinanceCharge" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Total Loan:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtTotalLoan" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell Font-Bold="true">
                                        Payment Amt:
                                    </asp:TableCell>
                                    <asp:TableCell>
                                        <telerik:RadNumericTextBox ID="txtPaymentAmt" AutoPostBack="true" NumberFormat-DecimalDigits="2" runat="server"></telerik:RadNumericTextBox>
                                    </asp:TableCell>
                                </asp:TableRow>
                                <asp:TableRow>
                                    <asp:TableCell ColumnSpan="2" HorizontalAlign="Right">
                                        <asp:Button ID="btnUpdate" runat="server" Text="Update" BackColor="#1eabe2" />
                                    </asp:TableCell>
                                </asp:TableRow>
                            </asp:Table>
                        </asp:TableCell>
                        <asp:TableCell>
                            &nbsp
                        </asp:TableCell>
                        <asp:TableCell VerticalAlign="Top">
                            <telerik:RadAjaxPanel ID="RadAjaxPanel1" runat="server" PostBackControls="rgClaim">
                                <telerik:RadGrid ID="rgClaim" runat="server" DataSourceID="dsAmortization" AutoGenerateColumns="false" AllowSorting="true">
                                    <MasterTableView AutoGenerateColumns="false" DataSourceID="dsAmortization" ShowFooter="true" ShowHeader="true"
                                        CommandItemDisplay="TopAndBottom">
                                        <CommandItemSettings ShowExportToExcelButton="true" ShowAddNewRecordButton="false" />
                                        <Columns>
                                            <telerik:GridBoundColumn DataField="ClaimGapAmortizationID" UniqueName="ClaimGapAmortizationID" Visible="false"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="PaymentNo" UniqueName="PaymentNo" HeaderText="Payment No"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="MonthDate" UniqueName="MonthDate" HeaderText="Month" DataFormatString="{0:M/d/yyyy}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Payment" UniqueName="Payment" HeaderText="Payment" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Interest" UniqueName="Interest" HeaderText="Interest" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="Principal" UniqueName="Principal" HeaderText="Principle" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="TotalInterest" UniqueName="TotalInterest" HeaderText="TotalInterest" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="BalRemaining" UniqueName="BalRemaining" HeaderText="Bal Remaining" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                            <telerik:GridBoundColumn DataField="LoanRemaining" UniqueName="LoanRemaining" HeaderText="Loan Remaining" DataFormatString="{0:N2}"></telerik:GridBoundColumn>
                                        </Columns>
                                    </MasterTableView>
                                </telerik:RadGrid>
                            </telerik:RadAjaxPanel>
                            <asp:SqlDataSource ID="dsAmortization" ProviderName="System.Data.SqlClient" 
                                SelectCommand="select ClaimGapAmortizationID, PaymentNo,MonthDate,Payment,Interest,
                                Principal,TotalInterest,BalRemaining,LoanRemaining from claimgapamortization where claimgapid = @claimid order by paymentno "
                                runat="server">
                                <SelectParameters>
                                    <asp:ControlParameter ControlID="hfClaimID" Name="ClaimID" PropertyName="Value" Type="Int32" />
                                </SelectParameters>
                            </asp:SqlDataSource>
                        </asp:TableCell>
                    </asp:TableRow>
                </asp:Table>
                
                <asp:HiddenField ID="hfClaimID" runat="server" />
                <asp:HiddenField ID="hfUserID" runat="server" />
                <asp:HiddenField ID="hfID" runat="server" />
            </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
