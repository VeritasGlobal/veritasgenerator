﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimVINTransmission1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("contractid")
        If Not IsPostBack Then
            GetVIN()
            If hfVIN.Value.Length > 0 Then
                FillPage()
            End If
        End If
    End Sub

    Private Sub GetVIN()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select vin from contract c "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfVIN.Value = clC.Fields("vin")
        End If
    End Sub

    Private Sub FillPage()
        Dim SQL As String
        Dim clV As New clsDBO
        SQL = "select * from vin.dbo.vin v "
        SQL = SQL + "inner join vin.dbo.transmission bd on bd.vinid = v.vinid "
        SQL = SQL + "where vin = '" & hfVIN.Value.Substring(0, 11) & "' "
        clV.OpenDB(SQL, AppSettings("connstring"))
        If clV.RowCount > 0 Then
            clV.GetRow()
            txtAvailability.Text = clV.Fields("availability")
            txtDetail.Text = clV.Fields("detailtype")
            txtGears.Text = clV.Fields("gears")
            txtName.Text = clV.Fields("name")
            txtOrderCode.Text = clV.Fields("ordercode")
            txtTransmissionType.Text = clV.Fields("transmissiontype")
            chkFleet.Checked = CBool(clV.Fields("fleet"))
        End If
    End Sub
End Class