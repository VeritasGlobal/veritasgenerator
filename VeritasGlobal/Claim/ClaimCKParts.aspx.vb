﻿Imports System.Configuration.ConfigurationManager
Imports Telerik.Web.UI

Public Class ClaimCKParts
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        dsPart.ConnectionString = AppSettings("connstring")
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            FillParts()
        End If
    End Sub

    Private Sub GetParts()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from ckquote "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount = 0 Then
            FillParts()
        Else
            clR.GetRow()
            If clR.Fields("orderid").Length > 0 Then
                btnPurchase.Visible = False
                btnRebuild.Visible = False
            End If
            txtOrderID.Text = clR.Fields("orderid")
            txtQuoteID.Text = clR.Fields("quoteid")
            txtTotalPartCost.Text = clR.Fields("partcost")
            txtShippingCost.Text = clR.Fields("shippingcost")
            txtTotalCost.Text = clR.Fields("totordercost")
            txtSentDate.Text = clR.Fields("sentdate")
            rgPayment.Rebind()
        End If

    End Sub

    Private Sub FillParts()
        Dim SQL As String
        Dim dCostAmt As Double = 0
        Dim lCKID As Long
        Dim clR As New clsDBO
        Dim clB As New clsDBO
        SQL = "select cl.claimid, cd.ClaimDetailID, cd.jobno, cd.reqamt, cd.reqQty,cdq.Qty, cdq.yourcost, cdq.PartNo, cdq.PartDesc from claim cl "
        SQL = SQL + "inner join ClaimDetail cd on cl.ClaimID = cd.ClaimID "
        SQL = SQL + "inner join ClaimDetailQuote cdq on cdq.ClaimDetailID = cd.ClaimDetailID "
        SQL = SQL + "where instock = 'yes' "
        SQL = SQL + "and cl.claimid = " & hfClaimID.Value & " "
        SQL = SQL + "and oem = 1"
        SQL = SQL + "group by cl.claimid, cd.claimdetailid, reqamt, YourCost, cdq.partno, cdq.partdesc, cd.JobNo, cd.reqQty,cdq.Qty "
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            For cnt = 0 To clR.RowCount - 1
                clR.GetRowNo(cnt)
                SQL = "select * from ckquote "
                SQL = SQL + "where claimid = " & hfClaimID.Value & " "
                clB.OpenDB(SQL, AppSettings("connstring"))
                If clB.RowCount > 0 Then
                    clB.GetRow()
                    If clB.Fields("orderid").Length > 0 Then
                        Exit Sub
                    End If
                    GoTo MoveHere
                Else
                    clB.NewRow()
                    clB.Fields("claimid") = hfClaimID.Value
                End If
                clB.Fields("quoteid") = ""
                clB.Fields("partcost") = 0
                clB.Fields("shippingcost") = 0
                clB.Fields("totalordercost") = 0
                If clB.RowCount = 0 Then
                    clB.AddRow()
                End If
                clB.SaveDB()
MoveHere:
                SQL = "select * from ckquote "
                SQL = SQL + "where claimid = " & hfClaimID.Value & " "
                clB.OpenDB(SQL, AppSettings("connstring"))
                If clB.RowCount > 0 Then
                    clB.GetRow()
                    lCKID = clB.Fields("ckid")
                End If
                SQL = "select * from ckpart "
                SQL = SQL + "where claimdetailid = " & clR.Fields("claimdetailid") & " "
                SQL = SQL + "and ckid = " & lCKID
                clB.OpenDB(SQL, AppSettings("connstring"))
                If clB.RowCount > 0 Then
                    clB.GetRow()
                Else
                    clB.NewRow()
                End If
                clB.Fields("ckid") = lCKID
                clB.Fields("claimdetailid") = clR.Fields("claimdetailid")
                clB.Fields("jobno") = clR.Fields("jobno")
                clB.Fields("partno") = clR.Fields("partno")
                clB.Fields("partname") = clR.Fields("partdesc")
                clB.Fields("qty") = clR.Fields("reqqty")
                clB.Fields("reqamt") = clR.Fields("reqamt")
                clB.Fields("yourcost") = clR.Fields("yourcost")
                clB.Fields("totalcost") = CDbl(clR.Fields("yourcost")) * CDbl(clR.Fields("reqqty"))
                dCostAmt = dCostAmt + (CDbl(clR.Fields("yourcost")) * CDbl(clR.Fields("reqqty")))
                If clB.RowCount = 0 Then
                    clB.AddRow()
                End If
                clB.SaveDB()
                txtTotalPartCost.Text = dCostAmt
                txtShippingCost.Text = 42.5
                txtTotalCost.Text = dCostAmt + 42.5
            Next

        End If
    End Sub

    Private Sub rgPayment_ItemCommand(sender As Object, e As GridCommandEventArgs) Handles rgPayment.ItemCommand
        Dim lDeleteID As Long
        Dim SQL As String
        Dim clR As New clsDBO

        If e.CommandName = "DeleteRow" Then
            lDeleteID = rgPayment.Items(e.Item.ItemIndex).GetDataKeyValue("ClaimDetailID")
            SQL = "delete from ckpart "
            SQL = SQL + "where claimdetailid = " & lDeleteID
            clR.RunSQL(SQL, AppSettings("connstring"))
        End If

    End Sub
End Class