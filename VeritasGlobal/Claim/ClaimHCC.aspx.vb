﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimHCC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("claimid")
        If Not IsPostBack Then
            GetServerInfo()
            trCEOApp1.Visible = False
            trManagerApp1.Visible = False
            trManagerApp2.visible = False
            trCEOApp2.Visible = False
            trInsurerApproval.Visible = False
            FillHCC()
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub FillHCC()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimhcc "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtAdjuster.Text = CalcUserName(clR.Fields("adjusterid"))
            If clR.Fields("datesubmitted").Length > 0 Then
                txtDateSubmitted.Text = Format(CDate(clR.Fields("datesubmitted")), "M/d/yyyy")
            Else
                txtDateSubmitted.Text = ""
            End If
            txtDaysIntoContract.Text = clR.Fields("DaysIntoContract")
            txtMilesIntoContract.Text = clR.Fields("MilesIntoContract")
            txtLimitofLiability.Text = clR.Fields("limitofliability")
            If clR.Fields("nadavalue").Length > 0 Then
                txtNADA.Text = Format(CDbl(clR.Fields("nadavalue")), "#,##0.00")
            Else
                txtNADA.Text = ""
            End If
            txtDescFailure.Text = clR.Fields("descfailure")
            If clR.Fields("claimcost").Length > 0 Then
                txtClaimCost.Text = Format(CDbl(clR.Fields("claimcost")), "#,##0.00")
            Else
                txtClaimCost.Text = ""
            End If
            If clR.Fields("laborcostperhour").Length > 0 Then
                txtLaborCostPerHour.Text = Format(CDbl(clR.Fields("laborcostperhour")), "#,##0.00")
            Else
                txtLaborCostPerHour.Text = ""
            End If
            If clR.Fields("averagecost").Length > 0 Then
                txtAverageCost.Text = Format(CDbl(clR.Fields("averagecost")), "#,##0.00")
            Else
                txtAverageCost.Text = ""
            End If
            txtRecommendCorrect.Text = clR.Fields("recommendcorrect")
            txtApprovedCorrect.Text = clR.Fields("approvedcorrect")
            If clR.Fields("originalroamount").Length > 0 Then
                txtOriginalROAmount.Text = Format(CDbl(clR.Fields("originalroamount")), "#,##0.00")
            Else
                txtOriginalROAmount.Text = ""
            End If
            If clR.Fields("mceapproveamount").Length > 0 Then
                txtMCEApproveAmount.Text = Format(CDbl(clR.Fields("MCEApproveAmount")), "#,##0.00")
            Else
                txtMCEApproveAmount.Text = ""
            End If
            chkInspectionDone.Checked = CBool(clR.Fields("inspectdone"))
            chkTeardownDone.Checked = CBool(clR.Fields("teardowndone"))
            chkRecordsRequestd.Checked = CBool(clR.Fields("recordsrequested"))
            chkCustomerStatementGiven.Checked = CBool(clR.Fields("customerstatementgiven"))
            chkPartsLaborMCE.Checked = CBool(clR.Fields("partslabormce"))
            chkInsurerApproval.Checked = CBool(clR.Fields("insurerapproval"))
            If chkInsurerApproval.Checked Then
                trInsurerApproval.Visible = True
            Else
                trInsurerApproval.Visible = False
            End If
            txtInsurerApproval.Content = clR.Fields("emailsent")
            txtExplainClaim.Text = clR.Fields("explainclaim")
            If clR.Fields("managerapprovalid").Length > 0 Then
                txtManagerApprovalPrint.Text = CalcUserName(clR.Fields("managerapprovalid"))
                txtManagerSign.Text = CalcUserName(clR.Fields("managerapprovalid"))
                trManagerApp1.Visible = True
                trManagerApp2.Visible = True
                btnManagerApproval.Enabled = False
            Else
                txtManagerSign.Text = ""
                txtManagerApprovalPrint.Text = ""
                btnManagerApproval.Enabled = True
                trManagerApp1.Visible = False
                trManagerApp2.Visible = False
            End If
            If clR.Fields("managerapprovaldate").Length > 0 Then
                txtManagerApprovalDate.Text = Format(CDate(clR.Fields("managerapprovaldate")), "M/d/yyyy")
            End If
            If clR.Fields("ceoapprovalid").Length > 0 Then
                txtCEOApprovalPrint.Text = CalcUserName(clR.Fields("ceoapprovalid"))
                txtCEOApprovalSign.Text = CalcUserName(clR.Fields("ceoapprovalid"))
                trCEOApp1.Visible = True
                trCEOApp2.Visible = True
                btnCEOApproval.Enabled = False
            Else
                txtCEOApprovalSign.Text = ""
                txtCEOApprovalPrint.Text = ""
                btnCEOApproval.Enabled = True
                trCEOApp1.Visible = False
                trCEOApp2.Visible = False
            End If
            If clR.Fields("ceoapprovaldate").Length > 0 Then
                txtCEOApprovalDate.Text = Format(CDate(clR.Fields("ceoapprovaldate")), "M/d/yyyy")
            End If
        End If
        hlWorksheet.NavigateUrl = "hcc.aspx?claimid=" & hfClaimID.Value
    End Sub

    Private Sub chkInsurerApproval_CheckedChanged(sender As Object, e As EventArgs) Handles chkInsurerApproval.CheckedChanged
        If chkInsurerApproval.Checked Then
            trInsurerApproval.Visible = True
        Else
            trInsurerApproval.Visible = False
        End If
    End Sub

    Private Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        If txtClaimCost.Text.Length = 0 Then
            lblError.Text = "Claim Cost needs to be entered."
            lblError.Visible = True
            Exit Sub
        Else
            lblError.Visible = False
        End If
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from claimHCC "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            clR.Fields("daysintocontract") = txtDaysIntoContract.Text
            clR.Fields("milesintocontract") = txtMilesIntoContract.Text
            clR.Fields("limitofliability") = txtLimitofLiability.Text
            clR.Fields("descfailure") = txtDescFailure.Text
            clR.Fields("claimcost") = txtClaimCost.Text
            clR.Fields("laborcostperhour") = txtLaborCostPerHour.Text
            clR.Fields("averagecost") = txtAverageCost.Text
            clR.Fields("recommendcorrect") = txtRecommendCorrect.Text
            clR.Fields("approvedcorrect") = txtApprovedCorrect.Text
            clR.Fields("originalroamount") = txtOriginalROAmount.Text
            clR.Fields("MCEApproveAmount") = txtMCEApproveAmount.Text
            clR.Fields("InspectDone") = chkInspectionDone.Checked
            clR.Fields("TearDownDone") = chkTeardownDone.Checked
            clR.Fields("RecordsRequested") = chkRecordsRequestd.Checked
            clR.Fields("CustomerStatementGiven") = chkCustomerStatementGiven.Checked
            clR.Fields("PartsLaborMCE") = chkPartsLaborMCE.Checked
            clR.Fields("InsurerApproval") = chkInsurerApproval.Checked
            clR.Fields("ExplainClaim") = txtExplainClaim.Text
            clR.Fields("EMailSent") = txtInsurerApproval.Content
            clR.Fields("NADAValue") = txtNADA.Text
            clR.SaveDB()
        End If
    End Sub

    Private Sub btnManagerApproval_Click(sender As Object, e As EventArgs) Handles btnManagerApproval.Click
        If txtClaimCost.Text.Length = 0 Then
            lblError.Text = "Claim Cost needs to be entered before approval."
            lblError.Visible = True
            Exit Sub
        Else
            lblError.Visible = False
        End If
        If Not CheckManagerApproval() Then
            lblError.Text = "You are not authorized to approve claim"
            lblError.Visible = True
            Exit Sub
        Else
            lblError.Visible = False
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimhcc "
        SQL = SQL + "set managerapprovalid = " & hfUserID.Value & ", "
        SQL = SQL + "managerapprovaldate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "select * from claimhcc "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("managerapprovalid").Length > 0 Then
                txtManagerApprovalPrint.Text = CalcUserName(clR.Fields("managerapprovalid"))
                txtManagerSign.Text = CalcUserName(clR.Fields("managerapprovalid"))
                trManagerApp1.Visible = True
                trManagerApp2.Visible = True
                btnManagerApproval.Enabled = False
            Else
                txtManagerSign.Text = ""
                txtManagerApprovalPrint.Text = ""
                btnManagerApproval.Enabled = True
                trManagerApp1.Visible = False
                trManagerApp2.Visible = False
            End If
            If clR.Fields("managerapprovaldate").Length > 0 Then
                txtManagerApprovalDate.Text = Format(CDate(clR.Fields("managerapprovaldate")), "M/d/yyyy")
            End If
        End If
    End Sub

    Private Sub btnCEOApproval_Click(sender As Object, e As EventArgs) Handles btnCEOApproval.Click
        If txtClaimCost.Text.Length = 0 Then
            lblError.Text = "Claim Cost needs to be entered before approval."
            lblError.Visible = True
            Exit Sub
        Else
            lblError.Visible = False
        End If
        If Not CheckCEOApproval() Then
            lblError.Text = "You are not authorized to approve claim"
            lblError.Visible = True
            Exit Sub
        Else
            lblError.Visible = False
        End If
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "update claimhcc "
        SQL = SQL + "set ceoapprovalid = " & hfUserID.Value & ", "
        SQL = SQL + "ceoapprovaldate = '" & Today & "' "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.RunSQL(SQL, AppSettings("connstring"))
        SQL = "select * from claimhcc "
        SQL = SQL + "where claimid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("ceoapprovalid").Length > 0 Then
                txtCEOApprovalPrint.Text = CalcUserName(clR.Fields("ceoapprovalid"))
                txtCEOApprovalSign.Text = CalcUserName(clR.Fields("ceoapprovalid"))
                trCEOApp1.Visible = True
                trCEOApp2.Visible = True
                btnCEOApproval.Enabled = False
            Else
                txtCEOApprovalSign.Text = ""
                txtCEOApprovalPrint.Text = ""
                btnCEOApproval.Enabled = True
                trCEOApp1.Visible = False
                trCEOApp2.Visible = False
            End If
            If clR.Fields("ceoapprovaldate").Length > 0 Then
                txtCEOApprovalDate.Text = Format(CDate(clR.Fields("ceoapprovaldate")), "M/d/yyyy")
            End If
        End If

    End Sub

    Private Function CheckCEOApproval() As Boolean
        CheckCEOApproval = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and hccapprovalceo <> 0 "
        SQL = SQL + "and claimauth > " & CDbl(txtClaimCost.Text)
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckCEOApproval = True
        End If
    End Function

    Private Function CheckManagerApproval() As Boolean
        CheckManagerApproval = False
        Dim clR As New clsDBO
        Dim SQL As String
        SQL = "select * from usersecurityinfo "
        SQL = SQL + "where userid = " & hfUserID.Value & " "
        SQL = SQL + "and hccapprovalmanager <> 0 "
        SQL = SQL + "and claimauth > " & CDbl(txtClaimCost.Text)
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            CheckManagerApproval = True
        End If
    End Function

    Private Function CalcUserName(xUserID As Long) As String
        Dim SQL As String
        Dim clU As New clsDBO
        CalcUserName = ""
        SQL = "select * from userinfo "
        SQL = SQL + "where userid = " & xUserID
        clU.OpenDB(SQL, AppSettings("connstring"))
        If clU.RowCount > 0 Then
            clU.GetRow()
            CalcUserName = clU.Fields("fname") & " " & clU.Fields("lname")
        End If
    End Function

End Class