﻿Imports Telerik.Web.UI
Imports System.Configuration.ConfigurationManager

Public Class ClaimGAPCalc
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfClaimID.Value = Request.QueryString("ClaimID")
        hfID.Value = Request.QueryString("sid")
        If Not IsPostBack Then
            GetServerInfo()
            fillcalc
        End If
    End Sub

    Private Sub FillCalc()
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapcalc "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            txtACV.Text = clR.Fields("acv")
            txtAmountExceding150.Text = clR.Fields("amountexceding150")
            txtCreditInsRefunds.Text = clR.Fields("creditinsrefunds")
            txtGAPPayout.Text = clR.Fields("gappayout")
            If clR.Fields("gappayout").Length > 0 Then
                If CDbl(txtGAPPayout.Text) < 0 Then
                    txtGAPPayout.Text = 0
                End If
            End If
            txtInsDeduct.Text = clR.Fields("insdeduct")
            txtInterestEarned.Text = clR.Fields("interestearned")
            txtLossLoanBalance.Text = clR.Fields("lossloanbalance")
            txtMechanicalContractRefund.Text = clR.Fields("MechanicalContractRefunds")
            txtNADALoss.Text = clR.Fields("nadaloss")
            txtOtherDeduct.Text = clR.Fields("otherdeduct")
            txtOwnerSalvage.Text = clR.Fields("OwnerSalvage")
            txtScheduleLoanBalance.Text = clR.Fields("ScheduleLoanBalance")
            txtTotalSchedule.Text = clR.Fields("TotalSchedule")
            txtUnearnedGAPRefund.Text = clR.Fields("UnearnedGAPRefund")
        Else
            txtACV.Text = 0
            txtAmountExceding150.Text = 0
            txtCreditInsRefunds.Text = 0
            txtGAPPayout.Text = 0
            txtInsDeduct.Text = 0
            txtInterestEarned.Text = 0
            txtLossLoanBalance.Text = 0
            txtMechanicalContractRefund.Text = 0
            txtNADALoss.Text = 0
            txtOtherDeduct.Text = 0
            txtOwnerSalvage.Text = 0
            txtScheduleLoanBalance.Text = 0
            txtTotalSchedule.Text = 0
            txtUnearnedGAPRefund.Text = 0
        End If
    End Sub

    Private Sub GetServerInfo()
        Dim SQL As String
        Dim clSI As New clsDBO
        Dim sStartDate As Date
        Dim sEndDate As Date
        sStartDate = Today
        sEndDate = DateAdd("d", 1, Today)
        hfID.Value = Request.QueryString("sid")
        SQL = "select * from serverinfo "
        SQL = SQL + "where systemid = '" & hfID.Value & "' "
        SQL = SQL + "and signindate >= '" & sStartDate & "' "
        SQL = SQL + "and signindate <= '" & sEndDate & "' "
        clSI.OpenDB(SQL, System.Configuration.ConfigurationManager.AppSettings("connstring"))
        If clSI.RowCount > 0 Then
            clSI.GetRow()
            hfUserID.Value = clSI.Fields("userid")
        End If
    End Sub

    Private Sub txtLossLoanBalance_TextChanged(sender As Object, e As EventArgs) Handles txtLossLoanBalance.TextChanged
        txtScheduleLoanBalance.Focus()
    End Sub

    Private Sub txtScheduleLoanBalance_TextChanged(sender As Object, e As EventArgs) Handles txtScheduleLoanBalance.TextChanged
        Dim SQL As String
        Dim clR As New clsDBO
        Dim sLossDate As String
        Dim sStartMonthDate As String
        Dim dStartInterest As Double
        Dim dEndInterest As Double
        Dim lDays As Long
        SQL = "select * from claimgap "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
            sLossDate = clR.Fields("lossdate")
            If sLossDate.Length > 0 Then
                SQL = "select * from claimgapamortization "
                SQL = SQL + "where claimgapid = " & hfClaimID.Value & " "
                SQL = SQL + "and monthdate < '" & sLossDate & "' "
                SQL = SQL + "order by paymentno desc "
                clR.OpenDB(SQL, AppSettings("connstring"))
                If clR.RowCount > 0 Then
                    clR.GetRow()
                    sStartMonthDate = clR.Fields("monthdate")
                    dStartInterest = clR.Fields("interest")
                    SQL = "select * from claimgapamortization "
                    SQL = SQL + "where claimgapid = " & hfClaimID.Value & " "
                    SQL = SQL + "and monthdate > '" & sLossDate & "' "
                    SQL = SQL + "order by paymentno "
                    clR.OpenDB(SQL, AppSettings("connstring"))
                    If clR.RowCount > 0 Then
                        clR.GetRow()
                        dEndInterest = clR.Fields("interest")
                        lDays = DateDiff(DateInterval.Day, CDate(sStartMonthDate), CDate(sLossDate))
                        txtInterestEarned.Text = Format((dStartInterest - dEndInterest) / CDbl(lDays), "#.00")

                    End If
                End If
            End If
        End If
        CalcTotalSchedule()
        CalcGAPPayout()
        txtACV.Focus()
    End Sub

    Private Sub CalcTotalSchedule()
        If txtScheduleLoanBalance.Text.Length > 0 Then
            txtTotalSchedule.Text = txtScheduleLoanBalance.Text
        End If
        If txtInterestEarned.Text.Length > 0 Then
            txtTotalSchedule.Text = CDbl(txtScheduleLoanBalance.Text) + CDbl(txtInterestEarned.Text)
        End If
    End Sub

    Private Sub CalcGAPPayout()
        txtGAPPayout.Text = 0
        If txtTotalSchedule.Text.Length > 0 Then
            txtGAPPayout.Text = txtTotalSchedule.Text
        End If
        If txtACV.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtACV.Text)
        End If
        If txtACV.Text.Length = 0 And txtNADALoss.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtNADALoss.Text)
        Else
            If txtACV.Text = "0" And txtNADALoss.Text.Length > 0 Then
                txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtNADALoss.Text)
            End If
        End If
        If txtOwnerSalvage.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtOwnerSalvage.Text)
        End If
        If txtMechanicalContractRefund.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtMechanicalContractRefund.Text)
        End If
        If txtCreditInsRefunds.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtCreditInsRefunds.Text)
        End If
        If txtUnearnedGAPRefund.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtUnearnedGAPRefund.Text)
        End If
        If txtInsDeduct.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtInsDeduct.Text)
        End If
        If txtAmountExceding150.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtAmountExceding150.Text)
        End If
        If txtInsDeduct.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtInsDeduct.Text)
        End If
        If txtOtherDeduct.Text.Length > 0 Then
            txtGAPPayout.Text = CDbl(txtGAPPayout.Text) - CDbl(txtOtherDeduct.Text)
        End If
        If CDbl(txtGAPPayout.Text) < 0 Then
            txtGAPPayout.Text = 0
        End If
    End Sub

    Private Sub txtACV_TextChanged(sender As Object, e As EventArgs) Handles txtACV.TextChanged
        CalcGAPPayout()
        txtNADALoss.Focus()
    End Sub

    Private Sub txtNADALoss_TextChanged(sender As Object, e As EventArgs) Handles txtNADALoss.TextChanged
        CalcGAPPayout()
        txtOwnerSalvage.Focus()
    End Sub

    Private Sub txtOwnerSalvage_TextChanged(sender As Object, e As EventArgs) Handles txtOwnerSalvage.TextChanged
        CalcGAPPayout()
        txtMechanicalContractRefund.Focus()
    End Sub

    Private Sub txtMechanicalContractRefund_TextChanged(sender As Object, e As EventArgs) Handles txtMechanicalContractRefund.TextChanged
        CalcGAPPayout()
        txtCreditInsRefunds.Focus()
    End Sub

    Private Sub txtCreditInsRefunds_TextChanged(sender As Object, e As EventArgs) Handles txtCreditInsRefunds.TextChanged
        CalcGAPPayout()
        txtUnearnedGAPRefund.Focus()
    End Sub

    Private Sub txtUnearnedGAPRefund_TextChanged(sender As Object, e As EventArgs) Handles txtUnearnedGAPRefund.TextChanged
        CalcGAPPayout()
        txtAmountExceding150.Focus()
    End Sub

    Private Sub txtInsDeduct_TextChanged(sender As Object, e As EventArgs) Handles txtInsDeduct.TextChanged
        CalcGAPPayout()
        txtOtherDeduct.Focus()
    End Sub

    Private Sub txtAmountExceding150_TextChanged(sender As Object, e As EventArgs) Handles txtAmountExceding150.TextChanged
        CalcGAPPayout()
        txtInsDeduct.Focus()
    End Sub

    Private Sub txtOtherPayout_TextChanged(sender As Object, e As EventArgs) Handles txtOtherDeduct.TextChanged
        CalcGAPPayout()
    End Sub

    Private Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim SQL As String
        Dim clR As New clsDBO
        SQL = "select * from claimgapcalc "
        SQL = SQL + "where claimgapid = " & hfClaimID.Value
        clR.OpenDB(SQL, AppSettings("connstring"))
        If clR.RowCount > 0 Then
            clR.GetRow()
        Else
            clR.NewRow()
            clR.Fields("claimgapid") = hfClaimID.Value
        End If
        clR.Fields("LossLoanBalance") = txtLossLoanBalance.Text
        clR.Fields("ScheduleLoanBalance") = txtScheduleLoanBalance.Text
        clR.Fields("InterestEarned") = txtInterestEarned.Text
        clR.Fields("TotalSchedule") = txtTotalSchedule.Text
        clR.Fields("ACV") = txtACV.Text
        clR.Fields("NADALoss") = txtNADALoss.Text
        clR.Fields("OwnerSalvage") = txtOwnerSalvage.Text
        clR.Fields("MechanicalContractRefunds") = txtMechanicalContractRefund.Text
        clR.Fields("CreditInsRefunds") = txtCreditInsRefunds.Text
        clR.Fields("UnearnedGAPRefund") = txtUnearnedGAPRefund.Text
        clR.Fields("AmountExceding150") = txtAmountExceding150.Text
        clR.Fields("InsDeduct") = txtInsDeduct.Text
        clR.Fields("OtherDeduct") = txtOtherDeduct.Text
        clR.Fields("GapPayout") = txtGAPPayout.Text
        If clR.RowCount = 0 Then
            clR.AddRow()
        End If
        clR.SaveDB()
    End Sub
End Class