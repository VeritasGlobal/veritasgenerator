﻿<%@ Page Language="vb" AutoEventWireup="false" CodeBehind="ClaimVINBasicData.aspx.vb" Inherits="VeritasGlobal.ClaimVINBasicData1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <style>
       * {
            font-family:Helvetica, Arial, sans-serif;
            font-size:small;
        }
    </style>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
        <asp:UpdatePanel runat="server">
                <ContentTemplate>
                    <asp:Table runat="server">
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Year:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtYear" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Make:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtMake" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Model:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtModel" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Trim:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtTrim" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Vehicle Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtVehicleType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Body Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtBodyType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Body Sub Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtBodySubType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                OEM Body Style:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtOEMBodyStyle" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Doors:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtDoors" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                OEM Doors:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtOEMDoors" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Model Number:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtModelNumber" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Package Code:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtPackageCode" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Package Summary:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtPackageSummary" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Rear Axle:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtRearAxle" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                DriveType:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtDriveType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Brake System:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtBrakeSystem" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Restraint Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtRestraintType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Country Of Mfr:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtCountryMfr" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Plant:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtPlant" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                            <asp:TableCell Font-Bold="true">
                                Chassis Type:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:TextBox ID="txtChassisType" ReadOnly="true" runat="server"></asp:TextBox>
                            </asp:TableCell>
                        </asp:TableRow>
                        <asp:TableRow>
                            <asp:TableCell Font-Bold="true">
                                Glider:
                            </asp:TableCell>
                            <asp:TableCell>
                                <asp:CheckBox ID="chkGlider" runat="server" />
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>

                    <asp:HiddenField ID="hfContractID" runat="server" />
                    <asp:HiddenField ID="hfClaimID" runat="server" />
                    <asp:HiddenField ID="hfVIN" runat="server" />
                </ContentTemplate>
        </asp:UpdatePanel>
    </form>
</body>
</html>
