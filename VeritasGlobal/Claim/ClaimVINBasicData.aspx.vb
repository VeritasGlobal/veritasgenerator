﻿Imports System.Configuration.ConfigurationManager

Public Class ClaimVINBasicData1
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        hfContractID.Value = Request.QueryString("ContractID")
        If Not IsPostBack Then
            GetVIN()
            If hfVIN.Value.Length > 0 Then
                FillPage()
            End If
        End If
    End Sub

    Private Sub GetVIN()
        Dim SQL As String
        Dim clC As New clsDBO
        SQL = "select vin from contract c "
        SQL = SQL + "where contractid = " & hfContractID.Value
        clC.OpenDB(SQL, AppSettings("connstring"))
        If clC.RowCount > 0 Then
            clC.GetRow()
            hfVIN.Value = clC.Fields("vin")
        End If
    End Sub

    Private Sub FillPage()
        Dim SQL As String
        Dim clV As New clsDBO

        SQL = "select * from vin.dbo.vin v "
        SQL = SQL + "inner join vin.dbo.basicdata bd on bd.vinid = v.vinid "
        SQL = SQL + "where vin = '" & hfVIN.Value.Substring(0, 11) & "' "
        clV.OpenDB(SQL, AppSettings("connstring"))
        If clV.RowCount > 0 Then
            clV.GetRow()
            txtYear.Text = clV.Fields("year")
            txtMake.Text = clV.Fields("make")
            txtModel.Text = clV.Fields("model")
            txtTrim.Text = clV.Fields("trim")
            txtVehicleType.Text = clV.Fields("vehicletype")
            txtBodyType.Text = clV.Fields("bodytype")
            txtBodySubType.Text = clV.Fields("bodysubtype")
            txtOEMBodyStyle.Text = clV.Fields("oembodystyle")
            txtDoors.Text = clV.Fields("door")
            txtOEMDoors.Text = clV.Fields("oemdoor")
            txtModelNumber.Text = clV.Fields("modelnumber")
            txtPackageCode.Text = clV.Fields("packagecode")
            txtPackageSummary.Text = clV.Fields("packagesummary")
            txtRearAxle.Text = clV.Fields("rearaxle")
            txtDriveType.Text = clV.Fields("drivetype")
            txtBrakeSystem.Text = clV.Fields("BrakeSystem")
            txtRestraintType.Text = clV.Fields("RestraintType")
            txtCountryMfr.Text = clV.Fields("countrymfr")
            txtPlant.Text = clV.Fields("plant")
            txtChassisType.Text = clV.Fields("chassistype")
            chkGlider.Checked = CBool(clV.Fields("glider"))
        End If
    End Sub
End Class