﻿Public Class clsMoxyDealID
    Private lContractID As Long
    Private lDealID As Long
    Private sDBType As String
    Public Property ContractID As Long
        Get
            ContractID = lContractID
        End Get
        Set(value As Long)
            lContractID = value
        End Set
    End Property

    Public Property DBType As String
        Get
            DBType = sDBType
        End Get
        Set(value As String)
            sDBType = value
        End Set
    End Property

    Public Property DealID As Long
        Get
            DealID = lDealID
        End Get
        Set(value As Long)
            lDealID = value
        End Set
    End Property

    Public Sub GetMoxyDealID()
        Dim SQL As String
        Dim clC As New clsDBO
        Dim clM As New clsDBO
        SQL = "select * from contract "
        SQL = SQL + "where contractid = " & lContractID
        clC.OpenDB(SQL, sCON)
        If clC.RowCount > 0 Then
            clC.GetRow()
            If clC.Fields("ContractNo").Substring(0, 3) = "RAC" Then
                lDealID = 0
                sDBType = ""
                Exit Sub
            End If
            If clC.Fields("contractno").Substring(0, 3) = "REP" Then
                SQL = "select * from VeritasMoxy.dbo.epcontract "
                SQL = SQL + "where contractno = '" & clC.Fields("contractno") & "' "
                sDBType = "EP"
                GoTo MoveHere
            End If
            If CLng(clC.Fields("programid")) > 46 And CLng(clC.Fields("programid")) < 61 Then
                SQL = "select * from VeritasMoxy.dbo.moxyproduct "
                SQL = SQL + "where contractno = '" & clC.Fields("contractno") & "' "
                DBType = "Product"
                GoTo MoveHere
            End If
            sDBType = "Normal"
            SQL = "select * from VeritasMoxy.dbo.moxycontract "
            SQL = SQL + "where contractno = '" & clC.Fields("contractno") & "' "
MoveHere:
        End If
        clM.OpenDB(SQL, sCON)
        If clM.RowCount = 0 Then
            lDealID = 0
            Exit Sub
        End If
        clM.GetRow()
        lDealID = clM.Fields("dealid")
    End Sub
End Class
