﻿

Public Class clsClaim
    Private sCon As String
    Private lClaimID As Long
    Private SQL As String
    Private lUserID As String
    Private sDatePaid As String

    Public Property CON As String
        Get
            CON = sCon
        End Get
        Set(value As String)
            sCon = value
        End Set
    End Property

    Public Property ClaimID As Long
        Get
            ClaimID = lClaimID
        End Get
        Set(value As Long)
            lClaimID = value
        End Set
    End Property

    Public Property DatePaid As String
        Get
            DatePaid = sDatePaid
        End Get
        Set(value As String)
            sDatePaid = value
        End Set
    End Property

    Public Property UserID As Long
        Get
            UserID = lUserID
        End Get
        Set(value As Long)
            lUserID = value
        End Set
    End Property

    Public Sub ProcessClaimStatus()
        Dim clR As New clsDBO
        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and claimdetailstatus <> 'Paid' "
        SQL = SQL + "and claimdetailstatus <> 'Cancelled' "
        SQL = SQL + "and claimdetailstatus <> 'denied' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            Exit Sub
        End If

        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and jobno like 'j%' "
        SQL = SQL + "and claimdetailstatus = 'Paid' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            SQL = "update claim "
            SQL = SQL + "set status = 'Paid', "
            SQL = SQL + "moddate = '" & Today & "', "
            SQL = SQL + "modby = " & lUserID & ", "
            SQL = SQL + "datepaid = '" & sDatePaid & "', "
            SQL = SQL + "paidby = " & lUserID & " "
            SQL = SQL + "where claimid = " & lClaimID & " "
            SQL = SQL + "and status = 'Open' "
            clR.RunSQL(SQL, sCon)
            Exit Sub
        End If

        SQL = "select * from claimdetail "
        SQL = SQL + "where claimid = " & lClaimID & " "
        SQL = SQL + "and jobno = 'A01' "
        clR.OpenDB(SQL, sCon)
        If clR.RowCount > 0 Then
            clR.GetRow()
            If clR.Fields("claimdetailstatus").ToLower.Trim = "denied" Then
                SQL = "update claim "
                SQL = SQL + "set status = 'Denied', "
                SQL = SQL + "moddate = '" & Today & "', "
                SQL = SQL + "modby = " & lUserID & " "
                SQL = SQL + "where claimid = " & lClaimID & " "
                SQL = SQL + "and status = 'Open'"
                clR.RunSQL(SQL, sCon)
            ElseIf clR.Fields("claimdetailstatus").ToLower.Trim = "cancelled" Then
                SQL = "update claim "
                SQL = SQL + "set status = 'Void', "
                SQL = SQL + "moddate = '" & Today & "', "
                SQL = SQL + "modby = " & lUserID & " "
                SQL = SQL + "where claimid = " & lClaimID & " "
                SQL = SQL + "and status = 'Open'"
                clR.RunSQL(SQL, sCon)
            Else
                SQL = "update claim "
                SQL = SQL + "set status = 'Paid', "
                SQL = SQL + "moddate = '" & Today & "', "
                SQL = SQL + "modby = " & lUserID & ", "
                SQL = SQL + "datepaid = '" & sDatePaid & "', "
                SQL = SQL + "paidby = " & lUserID & " "
                SQL = SQL + "where claimid = " & lClaimID & " "
                SQL = SQL + "and status = 'Open'"
                clR.RunSQL(SQL, sCon)
            End If
        End If

    End Sub
End Class
