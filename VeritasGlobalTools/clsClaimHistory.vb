﻿

Public Class clsClaimHistory
    Private lClaimID As Long
    Private sFieldName As String
    Private sPrevFieldInfo As String
    Private sFieldInfo As String
    Private lCreBy As Long

    Public Property ClaimID As Long
        Get
            ClaimID = lClaimID
        End Get
        Set(value As Long)
            lClaimID = value
        End Set
    End Property

    Public Property FieldName As String
        Get
            FieldName = sFieldName
        End Get
        Set(value As String)
            sFieldName = value
        End Set
    End Property

    Public Property PrevFieldInfo As String
        Get
            PrevFieldInfo = sPrevFieldInfo
        End Get
        Set(value As String)
            sPrevFieldInfo = value
        End Set
    End Property

    Public Property FieldInfo As String
        Get
            FieldInfo = sFieldInfo
        End Get
        Set(value As String)
            sFieldInfo = value
        End Set
    End Property

    Public Property CreBy As Long
        Get
            CreBy = lCreBy
        End Get
        Set(value As Long)
            lCreBy = value
        End Set
    End Property

    Public Sub AddClaimHistory()
        Dim SQL As String
        Dim clCH As New clsDBO
        SQL = "select * from claimhistory "
        SQL = SQL + "where claimhistoryid = 0 "
        clCH.OpenDB(SQL, sCON)
        If clCH.RowCount = 0 Then
            clCH.NewRow()
            clCH.Fields("claimid") = lClaimID
            clCH.Fields("fieldname") = sFieldName
            clCH.Fields("prevfieldinfo") = sPrevFieldInfo
            clCH.Fields("fieldinfo") = sFieldInfo
            clCH.Fields("credate") = Now
            clCH.Fields("creby") = lCreBy
            clCH.AddRow()
            clCH.SaveDB()
        End If
    End Sub

End Class
